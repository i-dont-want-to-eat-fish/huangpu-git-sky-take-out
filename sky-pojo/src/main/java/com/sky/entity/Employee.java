package com.sky.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@ApiModel("员工信息")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("员工ID")
    private Long id;

    //用户名
    @ApiModelProperty("员工用户名")
    private String username;

    //姓名
    @ApiModelProperty("员工姓名")
    private String name;

    //密码
    @ApiModelProperty("员工登录密码")
    private String password;

    //手机号
    @ApiModelProperty("员工手机号")
    private String phone;

    //性别 0 男 1 女
    @ApiModelProperty("员工性别")
    private Integer sex;

    //身份证号
    @ApiModelProperty("员工身份证号码")
    private String idNumber;

    //状态 1 正常  0  禁用
    @ApiModelProperty("员工状态")
    private Integer status;

    //创建时间
    @ApiModelProperty("创建时间")
    private LocalDateTime createTime;

    //修改时间
    @ApiModelProperty("修改时间")
    private LocalDateTime updateTime;

    //创建人
    @ApiModelProperty("创建人")
    private Long createUser;

    //修改人
    @ApiModelProperty("修改人")
    private Long updateUser;

}
