package com.sky.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@ApiModel("分页查询")
@NoArgsConstructor
@AllArgsConstructor
public class CategoryPageQueryDTO implements Serializable {

    //页码
    @ApiModelProperty("分类页码")
    private int page;

    //每页记录数
    @ApiModelProperty("分类分页条数")
    private int pageSize;

    //分类名称
    @ApiModelProperty("分类名称")
    private String name;

    //分类类型 1菜品分类  2套餐分类
    @ApiModelProperty("分类类型")
    private Integer type;

}
