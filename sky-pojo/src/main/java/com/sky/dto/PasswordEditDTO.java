package com.sky.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@ApiModel("密码修改请求对象")
@NoArgsConstructor
@AllArgsConstructor
public class PasswordEditDTO implements Serializable {

    //员工id
    @ApiModelProperty("员工ID")
    private Long empId;

    //旧密码
    @ApiModelProperty("员工登录旧密码")
    private String oldPassword;

    //新密码
    @ApiModelProperty("员工登录新密码")
    private String newPassword;

}
