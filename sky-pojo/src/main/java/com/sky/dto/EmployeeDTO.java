package com.sky.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@ApiModel("员工属性")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDTO implements Serializable {
    @ApiModelProperty("员工ID")
    private Long id;

    @ApiModelProperty("员工")
    private String username;

    @ApiModelProperty("员工姓名")
    private String name;

    @ApiModelProperty("员工电话")
    private String phone;

    @ApiModelProperty("员工性别")
    private Integer sex;

    @ApiModelProperty("员工身份证号码")
    private String idNumber;

}
