package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.RedisConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.exception.BusinessException;
import com.sky.mapper.CategoryMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.CategoryService;
import com.sky.utils.BeanHelper;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private SetmealMapper setmealMapper;

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private CategoryMapper categoryMapper;

    /**
     * 分类分页查询
     * @param categoryPageQueryDTO
     * @return
     */
    @Override
    public PageResult findByConstant(CategoryPageQueryDTO categoryPageQueryDTO) {

        PageHelper.startPage(categoryPageQueryDTO.getPage(),categoryPageQueryDTO.getPageSize());

        List<Category> categoryList=categoryMapper.findByConstant(categoryPageQueryDTO);

        Page p = (Page) categoryList;

        return new PageResult(p.getTotal(),p.getResult());

    }


    /**
     * 根据id查询员工
     * @param id
     * @return
     */
    @Override
    public Category findById(Long id) {

        Category category=categoryMapper.findById(id);

        return category;
    }


    /**
     * 修改分类
     * @param categoryDTO
     */
    @Override
    public void update(CategoryDTO categoryDTO) {

        Category category = Category.builder()
                .id(categoryDTO.getId())
                .name(categoryDTO.getName())
                .sort(categoryDTO.getSort())
                .type(categoryDTO.getType())
                .updateTime(LocalDateTime.now())
                .updateUser(BaseContext.getCurrentId())
                .build();

        categoryMapper.update(category);

        categoryDelete();

    }

    private void categoryDelete() {
        Set keys = redisTemplate.keys(RedisConstant.CATEGORY + "*");
        redisTemplate.delete(keys);
    }

    /**
     * 更新状态
     * @param status
     * @param id
     */
    @Override
    public void updateStatus(Integer status, Long id) {
        Category category = Category.builder()
                .id(id)
                .status(status)
                .updateTime(LocalDateTime.now())
                .updateUser(BaseContext.getCurrentId())
                .build();
        categoryMapper.update(category);

        categoryDelete();
    }

    /**
     * 新增分类
     * @param categoryDTO
     */
    @Override
    public void save(CategoryDTO categoryDTO) {

        Category category = BeanHelper.copyProperties(categoryDTO, Category.class);
        category.setUpdateTime(LocalDateTime.now());
        category.setCreateTime(LocalDateTime.now());
        category.setUpdateUser(BaseContext.getCurrentId());
        category.setCreateUser(BaseContext.getCurrentId());
        category.setStatus(StatusConstant.DISABLE);

        categoryMapper.insert(category);

        categoryDelete();

    }

    /**
     * 删除分类
     * @param id
     */
    @Override
    public void delete(Long id) {

        List<Setmeal> setmealList=setmealMapper.seacherSetmeal(id);

        List<Dish> dishList =dishMapper.seacherDish(id);

        if (ObjectUtils.isNotEmpty(setmealList)){
            throw new BusinessException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
        }

        if (ObjectUtils.isNotEmpty(dishList)){
            throw new BusinessException(MessageConstant.CATEGORY_BE_RELATED_BY_DISH);
        }
        categoryMapper.delete(id);

        categoryDelete();
    }

    /**
     * 根据类型查询分类
     * @param type
     * @return
     */
    @Override
    public List<Category> findByType(Integer type) {
        List<Category> categoryList=categoryMapper.findByType(type);
        return categoryList;
    }
}
