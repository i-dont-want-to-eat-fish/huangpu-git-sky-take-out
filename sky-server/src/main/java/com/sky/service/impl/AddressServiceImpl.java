package com.sky.service.impl;

import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.entity.AddressBook;
import com.sky.exception.BusinessException;
import com.sky.mapper.AddressMapper;
import com.sky.service.AddressService;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressMapper addressMapper;


    /**
     * 新增收货地址
     * @param addressBook
     */
    @Override
    public void add(AddressBook addressBook) {


        addressBook.setUserId(BaseContext.getCurrentId());
        addressBook.setCreateTime(LocalDateTime.now());
        addressBook.setIsDefault(0);

        addressMapper.add(addressBook);


    }


    /**
     * 查询地址
     * @return
     */
    @Override
    public List<AddressBook> find() {

        AddressBook addressBook = AddressBook.builder().userId(BaseContext.getCurrentId()).build();

        List<AddressBook> addressBookList=addressMapper.find(addressBook);

        return addressBookList;

    }


    /**
     * 设置默认地址
     * @param addressBook
     */
    @Override
    public void setDefaultAddress(AddressBook addressBook) {

        addressBook.setUserId(BaseContext.getCurrentId());

        //在设置默认地址前先去掉之前的默认值
        AddressBook book = AddressBook.builder().userId(BaseContext.getCurrentId()).build();
        List<AddressBook> addressBookList=addressMapper.find(book);

        for (AddressBook address : addressBookList) {

            if (address.getIsDefault()==1){
                address.setIsDefault(0);

                addressMapper.update(address);

            }
        }

        addressBook.setIsDefault(1);
        addressMapper.update(addressBook);

    }


    /**
     * 查询默认地址
     * @return
     */
    @Override
    public AddressBook findDefaultAddress() {

        AddressBook addressBook = AddressBook.builder()
                .userId(BaseContext.getCurrentId())
                .isDefault(1)
                .build();

        AddressBook address=addressMapper.findDefaultAddress(addressBook);

        return address;
    }


    /**
     * 根据id查询地址
     * @param id
     * @return
     */
    @Override
    public AddressBook findById(Long id) {

        AddressBook addressBook = AddressBook.builder()
                .userId(BaseContext.getCurrentId())
                .id(id)
                .build();

        List<AddressBook> addressBookList = addressMapper.find(addressBook);

        if (ObjectUtils.isEmpty(addressBookList)){
            throw new BusinessException(MessageConstant.UNKNOWN_ERROR);
        }

        AddressBook address = addressBookList.get(0);

        return address;
    }


    /**
     * 修改收货地址
     * @param addressBook
     */
    @Override
    public void update(AddressBook addressBook) {

        addressBook.setUserId(BaseContext.getCurrentId());

        addressMapper.update(addressBook);

    }


    /**
     * 根据id删除地址
     * @param addressBook
     */
    @Override
    public void delete(AddressBook addressBook) {

        addressBook.setUserId(BaseContext.getCurrentId());

        AddressBook defaultAddress = addressMapper.findDefaultAddress(addressBook);

        if (ObjectUtils.isNotEmpty(defaultAddress)){
            throw new BusinessException(MessageConstant.UNKNOWN_ERROR);
        }

        addressMapper.delete(addressBook);
    }
}
