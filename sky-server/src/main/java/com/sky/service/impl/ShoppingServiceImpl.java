package com.sky.service.impl;

import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.DishFlavor;
import com.sky.entity.ShoppingCart;
import com.sky.exception.BusinessException;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.ShoppingMapper;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.service.ShoppingService;
import com.sky.utils.BeanHelper;
import com.sky.vo.DishVO;
import com.sky.vo.SetmealVO;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Service
public class ShoppingServiceImpl implements ShoppingService {

    @Autowired
    private SetmealMapper setmealMapper;

    @Autowired
    private DishService dishService;


    @Autowired
    private ShoppingMapper shoppingMapper;


    /**
     * 添加购物车
     * @param shoppingCartDTO
     */
    @Override
    public void add(ShoppingCartDTO shoppingCartDTO) {

        //1.查询当前购物车，是否有当前要添加的商品（包括菜品，口味 或套餐）
        ShoppingCart shoppingCart = BeanHelper.copyProperties(shoppingCartDTO, ShoppingCart.class);
        shoppingCart.setUserId(BaseContext.getCurrentId());

        List<ShoppingCart> shoppingCartList =shoppingMapper.list(shoppingCart);

        //2.如果当前商品存在购物车，则商品加一
        if (ObjectUtils.isNotEmpty(shoppingCartList)){
            ShoppingCart cart = shoppingCartList.get(0);
            cart.setNumber(cart.getNumber()+1);
            shoppingMapper.updateNumberByid(cart);
        }

        //3.如果当前商品不存在，则新增

        if (ObjectUtils.isEmpty(shoppingCartList)){

            Long dishId = shoppingCart.getDishId();

            if (dishId==null){

                Long setmealId = shoppingCart.getSetmealId();

                SetmealVO setmealVO = setmealMapper.seacherSetmealById(Collections.singletonList(setmealId));
                shoppingCart.setName(setmealVO.getName());
                shoppingCart.setImage(setmealVO.getImage());
                shoppingCart.setAmount(setmealVO.getPrice());

            }

            if (dishId!=null){

                DishVO dishVO = dishService.findById(dishId);
                shoppingCart.setName(dishVO.getName());
                shoppingCart.setImage(dishVO.getImage());
                shoppingCart.setAmount(dishVO.getPrice());

            }

            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCart.setNumber(1);

            shoppingMapper.insert(shoppingCart);
        }

    }


    /**
     * 查询购物车
     * @return
     */
    @Override
    public List<ShoppingCart> find() {

        ShoppingCart cart = ShoppingCart.builder().userId(BaseContext.getCurrentId()).build();

        List<ShoppingCart> shoppingCartList = shoppingMapper.list(cart);

        return shoppingCartList;
    }


    /**
     * 删除商品
     * @param shoppingCart
     */
    @Override
    public void delete(ShoppingCart shoppingCart) {

        shoppingCart.setUserId(BaseContext.getCurrentId());

        List<ShoppingCart> carts = shoppingMapper.list(shoppingCart);



        if (ObjectUtils.isEmpty(carts)){
            throw new BusinessException(MessageConstant.UNKNOWN_ERROR);
        }

        Integer number = carts.get(0).getNumber();

        if (ObjectUtils.isEmpty(number)){
            throw new BusinessException(MessageConstant.UNKNOWN_ERROR);
        }


        if (number>1){

            shoppingCart.setNumber(number-1);

            shoppingMapper.updateNumberByid(shoppingCart);
        }

        if (number==1){

            shoppingMapper.delete(shoppingCart);
        }

    }


    /**
     * 清空购物车
     */
    @Override
    public void deleteAll() {
        ShoppingCart shoppingCart= ShoppingCart.builder().userId(BaseContext.getCurrentId()).build();

        shoppingMapper.delete(shoppingCart);

    }
}
