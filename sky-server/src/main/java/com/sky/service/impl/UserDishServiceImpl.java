package com.sky.service.impl;

import com.sky.constant.RedisConstant;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.mapper.DishFlavorsMapper;
import com.sky.mapper.UserDishMapper;
import com.sky.service.UserDishService;
import com.sky.utils.BeanHelper;
import com.sky.vo.DishVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class UserDishServiceImpl implements UserDishService {

    @Autowired
    private DishFlavorsMapper dishFlavorsMapper;

    @Autowired
    private UserDishMapper userDishMapper;
    /**
     * 根据分类id查询菜品
     * @param categoryId
     * @return
     */
    @Cacheable(cacheNames = RedisConstant.DISH,key ="#p0")
    @Override
    public List<DishVO> findDishByCategoryId(Integer categoryId) {


        //查询菜品信息
        List<Dish> dishes=userDishMapper.findDishByCategoryId(categoryId);

        //根据dishID查询flovars口味


        List<DishVO> dishVOs =new ArrayList<>();

        for (Dish dish : dishes) {

            List<DishFlavor> Flavor = dishFlavorsMapper.findByDishId(dish.getId());

            DishVO dishVO = BeanHelper.copyProperties(dish, DishVO.class);

            dishVO.setFlavors(Flavor);

            dishVOs.add(dishVO);

        }

        return dishVOs;
    }
}
