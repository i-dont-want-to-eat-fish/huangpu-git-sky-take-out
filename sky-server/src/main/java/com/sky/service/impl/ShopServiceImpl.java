package com.sky.service.impl;

import com.sky.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class ShopServiceImpl implements ShopService {

    @Autowired
    private RedisTemplate redisTemplate;


    /**
     * 设置店铺运营状态
     * @param status
     */
    @Override
    public void updateStatus(Integer status) {
        redisTemplate.opsForValue().set("status",status);
    }

    /**
     * 店铺运营状态
     */
    @Override
    public Integer getStatus() {

        Integer status=(Integer) redisTemplate.opsForValue().get("status");
        return status;

    }
}
