package com.sky.service.impl;

import com.sky.dto.OrderReportDTO;
import com.sky.dto.SalesReportDTO;
import com.sky.dto.TurnoverReportDTO;
import com.sky.dto.UserReportDTO;
import com.sky.entity.Orders;
import com.sky.mapper.OrderMapper;
import com.sky.mapper.UserMapper;
import com.sky.service.ReportService;
import com.sky.vo.*;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    private UserMapper userMapper;


    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private HttpServletResponse response;


    /**
     * 营业额统计
     * @param begin
     * @param end
     * @return
     */
    @Override
    public TurnoverReportVO turnoverStatistics(LocalDate begin, LocalDate end) {

        //1、获得时间列表（String）
        List<String> dateList = getDateList(begin, end);

        //2、获取营业额列表（BigDecimal）

        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);
        List<TurnoverReportDTO> turnoverReportDTOList=orderMapper.findTurnoverByDate(beginTime,endTime, Orders.ORDER_STAUTS_COMPLETED);
        //2.1、获取Map集合
        Map<String, BigDecimal> dateMap = turnoverReportDTOList.stream().collect(Collectors.toMap(TurnoverReportDTO::getOrderDate, TurnoverReportDTO::getOrderMoney));


        //2.2、处理null值数据，要与时间列表匹配
        List<BigDecimal> turnoverList =new ArrayList<>();
        for (String s : dateList) {
            turnoverList.add(dateMap.get(s) == null ? new BigDecimal("0") : dateMap.get(s));
        }


        return new TurnoverReportVO(dateList,turnoverList);
    }


    /**
     * 用户统计
     * @param begin
     * @param end
     * @return
     */
    @Override
    public UserReportVO userStatistics(LocalDate begin, LocalDate end) {

        //获取时间列表
        List<String> dateList = getDateList(begin, end);

        //获取新增用户
        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);
        List<UserReportDTO>  userReportDTOList =userMapper.getNewUserListByDate(beginTime, endTime);
        Map<String, Integer> userMap = userReportDTOList.stream().collect(Collectors.toMap(UserReportDTO::getCreateDate, UserReportDTO::getUserCount));

        List<Integer> newUserList = dateList.stream().map(e -> userMap.get(e) == null ? 0 : userMap.get(e)).collect(Collectors.toList());

        //获取用户总量
        Integer userListBefore=userMapper.getUserListBeforeTime(beginTime);

        List<Integer> totalUserList =new ArrayList<>();

        for (Integer newUser : newUserList) {

            userListBefore+=newUser;

            totalUserList.add(userListBefore);
        }


        return new UserReportVO(dateList,totalUserList,newUserList);
    }


    /**
     * 订单统计
     * @param begin
     * @param end
     * @return
     */
    @Override
    public OrderReportVO ordersStatistics(LocalDate begin, LocalDate end) {

        //1、日期列表
        List<String> dateList = getDateList(begin, end);

        //2、每日订单数
        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);

        List<OrderReportDTO> orderList =orderMapper.findOrderList(beginTime,endTime,null);
        Map<String, Integer> orderMap = orderList.stream().collect(Collectors.toMap(OrderReportDTO::getOrderDate, OrderReportDTO::getOrderCount));
        List<Integer> orderCountList = dateList.stream().map(e -> orderMap.get(e) == null ? 0 : orderMap.get(e)).collect(Collectors.toList());

        //3、每日有效订单数
        List<OrderReportDTO> validOrderList =orderMapper.findOrderList(beginTime,endTime,Orders.ORDER_STAUTS_COMPLETED);
        Map<String, Integer> validOrdrerMap = validOrderList.stream().collect(Collectors.toMap(OrderReportDTO::getOrderDate, OrderReportDTO::getOrderCount));
        List<Integer> validOrderCountList = dateList.stream().map(e -> validOrdrerMap.get(e) == null ? 0 : validOrdrerMap.get(e)).collect(Collectors.toList());

        //4、订单总数
        Integer totalOrderCount = orderCountList.stream().reduce(Integer::sum).get();

        //5、有效订单总数
        Integer validOrderCount = validOrderCountList.stream().reduce(Integer::sum).get();
        //6、订单完成率

        Double orderCompletionRate;

        if (totalOrderCount==0){
            orderCompletionRate = 0.0;
        }else {
            orderCompletionRate =validOrderCount*1.0/totalOrderCount;
        }

        return new OrderReportVO(dateList,orderCountList,validOrderCountList,totalOrderCount,validOrderCount,orderCompletionRate);

    }


    /**
     * 查询销量排名top10
     * @param begin
     * @param end
     * @return
     */
    @Override
    public SalesTop10ReportVO top(LocalDate begin, LocalDate end) {
        //获取时间列表
        List<String> dateList = getDateList(begin, end);

        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);

        List<SalesReportDTO> salesReportDTOList=orderMapper.findValidDish(beginTime,endTime,Orders.ORDER_STAUTS_COMPLETED);


        //数据封装
        List<String> nameList = salesReportDTOList.stream().map(e -> e.getGoodsName()).collect(Collectors.toList());
        List<Integer> numberList = salesReportDTOList.stream().map(e -> e.getGoodsNumber()).collect(Collectors.toList());

        return new SalesTop10ReportVO(nameList,numberList);


    }

    private List<String> getDateList(LocalDate begin, LocalDate end) {

        List<LocalDate> dates = begin.datesUntil(end.plusDays(1)).collect(Collectors.toList());
        List<String> dateList = dates.stream().map(e -> e.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))).collect(Collectors.toList());

        return dateList;
    }


    /**
     * 导出Excel报表
     */
    @Override
    public void export() throws Exception {

        //加载已有模
        InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("templates/运营数据报表模板.xlsx");

        Workbook workbook = new XSSFWorkbook(resourceAsStream);

        //加载总览表格
        Sheet sheet = workbook.getSheetAt(0);

        //填充总览表格

        LocalDate begin = LocalDate.now().minusDays(30);
        LocalDate end = LocalDate.now().minusDays(1);

        LocalDateTime beginTime = LocalDateTime.of(begin, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(end, LocalTime.MAX);

        BusinessDataVO businessDataVO = orderMapper.findAllDetail(beginTime, endTime);

        Integer newUserCount = userMapper.findByTime(beginTime, endTime);

        String date = begin+"到"+end+"数据分析表";

        sheet.getRow(1).getCell(1).setCellValue(date);
        sheet.getRow(3).getCell(2).setCellValue(businessDataVO.getTurnover().toString());
        sheet.getRow(3).getCell(4).setCellValue(businessDataVO.getOrderCompletionRate().toString());
        sheet.getRow(3).getCell(6).setCellValue(newUserCount.toString());
        sheet.getRow(4).getCell(2).setCellValue(businessDataVO.getValidOrderCount().toString());
        sheet.getRow(4).getCell(4).setCellValue(businessDataVO.getUnitPrice().toString());

        //加载详细表格
        for (int i = 0; i < 30; i++) {
            Row row = sheet.getRow(7 + i);

            LocalDate everyDate = begin.plusDays(i);

            LocalDateTime time = LocalDateTime.of(everyDate, LocalTime.MIN);
            LocalDateTime timeEnd = LocalDateTime.of(everyDate, LocalTime.MAX);

            BusinessDataVO allDetail = orderMapper.findAllDetail(time, timeEnd);

            Integer newUser = userMapper.findByTime(time, endTime);

            row.getCell(1).setCellValue(begin.plusDays(i));
            row.getCell(2).setCellValue(allDetail.getTurnover().toString());
            row.getCell(3).setCellValue(allDetail.getValidOrderCount().toString());
            row.getCell(4).setCellValue(allDetail.getOrderCompletionRate().toString());
            row.getCell(5).setCellValue(allDetail.getUnitPrice().toString());
            row.getCell(6).setCellValue(newUser.toString());
        }

        //响应数据

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);

        //释放资源

        outputStream.close();
        workbook.close();

    }

}
