package com.sky.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.dto.OrdersPaymentDTO;
import com.sky.dto.OrdersSubmitDTO;
import com.sky.entity.*;
import com.sky.exception.BusinessException;
import com.sky.mapper.AddressMapper;
import com.sky.mapper.OrderMapper;
import com.sky.mapper.ShoppingMapper;
import com.sky.mapper.UserMapper;
import com.sky.result.LocalAddress;
import com.sky.result.PageResult;
import com.sky.service.OrderService;
import com.sky.socket.WebSocket;
import com.sky.utils.BaiduMapUtils;
import com.sky.utils.BeanHelper;
import com.sky.utils.WeChatPayUtil;
import com.sky.vo.OrderPaymentVO;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private AddressMapper addressMapper;

    @Autowired
    private ShoppingMapper shoppingMapper;

    @Autowired
    private WeChatPayUtil weChatPayUtil;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private WebSocket webSocket;

    @Autowired
    private BaiduMapUtils baiduMapUtils;

    @Value("${sky.shop.address}")
    private String address;


    /**
     * 提交订单
     * @param ordersSubmitDTO
     * @return
     */
    @Override
    public OrderSubmitVO submit(OrdersSubmitDTO ordersSubmitDTO) throws Exception {


        //封装订单数据
        Long addressBookId = ordersSubmitDTO.getAddressBookId();

        AddressBook addressBook = AddressBook.builder().userId(BaseContext.getCurrentId()).id(addressBookId).build();

        AddressBook addr = addressMapper.find(addressBook).get(0);

        if (ObjectUtils.isEmpty(addr)){
            throw new BusinessException(MessageConstant.ADDRESS_BOOK_IS_NULL);
        }

        //校验收货地址是否超过配送范围
        LocalAddress destination = baiduMapUtils.getAddress(addr.getProvinceName() + addr.getCityName() + addr.getDistrictName() + addr.getDetail());

        LocalAddress origin = baiduMapUtils.getAddress(address);

        Integer distance = baiduMapUtils.getDistance(origin, destination);

        if (distance>=5000){
            throw new BusinessException(MessageConstant.DELIVERY_OUT_OF_RANGE);
        }


        Orders orders = BeanHelper.copyProperties(ordersSubmitDTO, Orders.class);

        //订单号
        orders.setNumber(String.valueOf(System.nanoTime()));


        //订单状态
        orders.setStatus(1);//待付款

        //用户id
        orders.setUserId(BaseContext.getCurrentId());

        //下单时间
        orders.setOrderTime(LocalDateTime.now());

        //下单手机号
        orders.setPhone(addr.getPhone());

        //
        orders.setAddress(addr.getDetail());

        //收货人
        orders.setConsignee(addr.getConsignee());


        orders.setPayStatus(0);


        orderMapper.insertOrders(orders);

        //封装订单详细


        ShoppingCart shoppingCart = ShoppingCart.builder().userId(BaseContext.getCurrentId()).build();


        List<ShoppingCart> shoppingCartList = shoppingMapper.list(shoppingCart);
        if (ObjectUtils.isEmpty(shoppingCartList)){
            throw new BusinessException(MessageConstant.UNKNOWN_ERROR);
        }

        for (ShoppingCart cart : shoppingCartList) {

            OrderDetail orderDetail = BeanHelper.copyProperties(cart, OrderDetail.class);

            orderDetail.setOrderId(Long.valueOf(orders.getNumber()));

            orderMapper.insertOrderDetail(orderDetail);

        }


        //清空购物车

        shoppingMapper.delete(ShoppingCart.builder().userId(BaseContext.getCurrentId()).build());

        //封装返回值


        OrderSubmitVO orderSubmitVO = OrderSubmitVO.builder()
                .id(orders.getId())
                .orderNumber(orders.getNumber())
                .orderAmount(orders.getAmount())
                .orderTime(orders.getOrderTime()).build();


        //提醒用户接单
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("type",1);
        paramMap.put("orderId",orders.getId());
        paramMap.put("content",orders.getNumber());

        webSocket.sendMessageToAllClient(JSONObject.toJSONString(paramMap));

        return orderSubmitVO;

    }



    /**
     * 订单支付
     *
     * @param ordersPaymentDTO
     * @return
     */
    public OrderPaymentVO payment(OrdersPaymentDTO ordersPaymentDTO) throws Exception {
        // 当前登录用户id
        Long userId = BaseContext.getCurrentId();
        User user = userMapper.getById(userId);

        //调用微信支付接口，生成预支付交易单
        JSONObject jsonObject = weChatPayUtil.pay(
                ordersPaymentDTO.getOrderNumber(), //商户订单号
                new BigDecimal(0.01), //支付金额，单位 元
                "苍穹外卖订单", //商品描述
                user.getOpenid() //微信用户的openid
        );

        if (jsonObject.getString("code") != null && jsonObject.getString("code").equals("ORDERPAID")) {
            throw new BusinessException("该订单已支付");
        }

        OrderPaymentVO vo = jsonObject.toJavaObject(OrderPaymentVO.class);
        vo.setPackageStr(jsonObject.getString("package"));

        return vo;
    }
    /**
     * 支付成功，修改订单状态
     *
     * @param outTradeNo
     */
    public void paySuccess(String outTradeNo) {
        // 根据订单号查询当前用户的订单
        Orders ordersDB = orderMapper.getByNumber(outTradeNo);

        // 根据订单id更新订单的状态、支付方式、支付状态、结账时间
        Orders orders = Orders.builder()
                .id(ordersDB.getId())
                .status(Orders.ORDER_STAUTS_TO_BE_CONFIRMED)
                .payStatus(Orders.PAY_STATUS_PAID)
                .checkoutTime(LocalDateTime.now())
                .build();

        orderMapper.update(orders);
    }

    /**
     * 查询历史订单记录
     * @param ordersPageQueryDTO
     * @return
     */
    @Override
    public PageResult findOrder(OrdersPageQueryDTO ordersPageQueryDTO) {

        PageHelper.startPage(ordersPageQueryDTO.getPage(),ordersPageQueryDTO.getPageSize());

        List<OrderVO> ordersList=orderMapper.findOrder(ordersPageQueryDTO.getStatus());

        if (ObjectUtils.isEmpty(ordersList)){
            return null;

        }

        for (OrderVO orders : ordersList) {

            String orderId = orders.getNumber();

            List<OrderDetail> orderDetails=orderMapper.findDetailByOrderId(orderId);

            orders.setOrderDetailList(orderDetails);
        }


        Page p = (Page) ordersList;

        return new PageResult(p.getTotal(),p.getResult());

    }


    /**
     * 查询订单详情
     * @param id
     * @return
     */
    @Override
    public OrderVO findOrderDetail(Long id) {

        OrderVO orderVO=orderMapper.findOrderDetail(id);

        String orderId = orderVO.getNumber();

        List<OrderDetail> orderDetails=orderMapper.findDetailByOrderId(orderId);

        orderVO.setOrderDetailList(orderDetails);

        return orderVO;
    }


    /**
     * 取消订单
     * @param id
     */
    @Override
    public void cancel(Long id) {

        Orders orders=orderMapper.findOrderDetail(id);

        if (ObjectUtils.isEmpty(orders)){
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }

        if (!(orders.getStatus()==1 && orders.getStatus()==2)){
            throw new BusinessException(MessageConstant.UNKNOWN_ERROR);
        }



        //TODO  weChatPayUtil.refund();

        Orders build = Orders.builder().id(id).status(6).userId(BaseContext.getCurrentId()).build();

        orderMapper.update(build);

    }

    /**
     * 再来一单
     * @param id
     */
    @Override
    public void again(Long id) {

        OrderVO orderVO=orderMapper.findOrderDetail(id);

        String orderId = orderVO.getNumber();

        List<OrderDetail> orderDetails=orderMapper.findDetailByOrderId(orderId);


        for (OrderDetail orderDetail : orderDetails) {

            ShoppingCart shoppingCart = BeanHelper.copyProperties(orderDetail, ShoppingCart.class);

            shoppingCart.setUserId(BaseContext.getCurrentId());
            shoppingCart.setCreateTime(LocalDateTime.now());

            shoppingMapper.insert(shoppingCart);

        }

    }

}
