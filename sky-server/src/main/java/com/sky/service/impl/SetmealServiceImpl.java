package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.RedisConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.SetmealDTO;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.exception.BusinessException;
import com.sky.mapper.DishAndSetmealMapper;
import com.sky.mapper.DishMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.result.PageResult;
import com.sky.service.SetmealService;
import com.sky.utils.BeanHelper;
import com.sky.vo.SetmealVO;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Service
public class SetmealServiceImpl implements SetmealService {

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private SetmealMapper setmealMapper;

    @Autowired
    private DishAndSetmealMapper dishAndSetmealMapper;

    /**
     * 新增套餐
     * @param setmealDTO
     */
    @Transactional
    @Override
    public void save(SetmealDTO setmealDTO) {

        //创建套餐
        Setmeal setmeal = BeanHelper.copyProperties(setmealDTO, Setmeal.class);
        setmeal.setStatus(StatusConstant.DISABLE);
        setmealMapper.save(setmeal);


        //新增套餐内菜品
        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();
        if (ObjectUtils.isNotEmpty(setmealDishes)){
            //注入
            setmealDishes.forEach(e-> e.setSetmealId(setmeal.getId()));
            dishAndSetmealMapper.save(setmealDishes);
        }

        /*//主键返回拿到setmeal的id
        Long id = setmeal.getId();

        List<Long> dishIds=dishAndSetmealMapper.findDishId(id);*/

    }

    /**
     * 分页查询
     * @param setmealPageQueryDTO
     * @return
     */
    @Override
    public PageResult page(SetmealPageQueryDTO setmealPageQueryDTO) {
        //设置分页参数
        PageHelper.startPage(setmealPageQueryDTO.getPage(),setmealPageQueryDTO.getPageSize());
        List<SetmealVO> setmealList=setmealMapper.page(setmealPageQueryDTO);

        Page p = (Page) setmealList;

        return new PageResult(p.getTotal(),p.getResult());
    }

    /**
     * 批量删除套餐
     * @param ids
     */
    @CacheEvict(cacheNames = RedisConstant.SETMEAL,allEntries = true)
    @Transactional
    @Override
    public void deleteById(List<Long> ids) {

        //起售不能删除
        SetmealVO setmealVO =setmealMapper.seacherSetmealById(ids);
        if (setmealVO.getStatus().equals(StatusConstant.ENABLE)){
            throw new BusinessException(MessageConstant.SETMEAL_ON_SALE);
        }

        //删除菜品和套餐关联表的信息
        dishAndSetmealMapper.deleteBysetmealIds(ids);

        //删除套餐
        setmealMapper.deleteById(ids);
    }
    /**
     * 根据id查询套餐
     * @param id
     * @return
     */
    @Override
    public SetmealVO findById(Long id) {


        SetmealVO setmealVO = setmealMapper.seacherSetmealById(Collections.singletonList(id));

        List<SetmealDish> setmealDishes = dishAndSetmealMapper.findBySetmealId(id);

        setmealVO.setSetmealDishes(setmealDishes);

        return setmealVO;
    }

    /**
     * 修改套餐
     * @param setmealDTO
     */
    @CacheEvict(cacheNames = RedisConstant.SETMEAL,allEntries = true)
    @Transactional
    @Override
    public void update(SetmealDTO setmealDTO) {

        Setmeal setmeal = BeanHelper.copyProperties(setmealDTO, Setmeal.class);
        setmealMapper.update(setmeal);

        List<SetmealDish> setmealDishes = setmealDTO.getSetmealDishes();

        if (ObjectUtils.isNotEmpty(setmealDishes)){

            Long id = setmealDTO.getId();
            setmealDishes.forEach(e-> e.setSetmealId(id));

            dishAndSetmealMapper.deleteBysetmealIds(Collections.singletonList(id));

            dishAndSetmealMapper.save(setmealDishes);
        }
    }

    /**
     * 套餐起售、停售
     * @param setmeal
     */
    @CacheEvict(cacheNames = RedisConstant.SETMEAL,allEntries = true)
    @Transactional
    @Override
    public void updateStatus(Setmeal setmeal) {

        if (StatusConstant.ENABLE.equals(setmeal.getStatus())){

            Long id = setmeal.getId();

            List<Long> dishIds= dishAndSetmealMapper.seacherDishId(id);

            dishIds.forEach(e-> {

                Dish dishId = dishMapper.findById(e);

                if (StatusConstant.DISABLE.equals(dishId.getStatus())){
                    throw new BusinessException(MessageConstant.SETMEAL_ENABLE_FAILED);
                }
            });
        }
        setmealMapper.update(setmeal);
    }
}
