package com.sky.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sky.constant.MessageConstant;
import com.sky.dto.UserLoginDTO;
import com.sky.entity.User;
import com.sky.exception.BusinessException;
import com.sky.mapper.UserMapper;
import com.sky.properties.WeChatProperties;
import com.sky.service.UserService;
import com.sky.utils.HttpClientUtil;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {


    private  static final String WEXIN_LOGIN_URL="https://api.weixin.qq.com/sns/jscode2session";

    @Autowired
    private WeChatProperties weChatProperties;

    @Autowired
    private UserMapper userMapper;

    /**
     * 微信登录
     * @param userLoginDTO
     * @return
     */
    @Override
    public User login(UserLoginDTO userLoginDTO) {
        //1、调用HttpClient(微信接口)，实现登录

        Map<String, String> paramMap = new HashMap<>();

        paramMap.put("js_code",userLoginDTO.getCode());
        paramMap.put("appid",weChatProperties.getAppid());
        paramMap.put("secret",weChatProperties.getSecret());
        paramMap.put("grant_type","authorization_code");


        String jsonResult = HttpClientUtil.doGet(WEXIN_LOGIN_URL, paramMap);

        //对jsonResult的非空判断
        if (ObjectUtils.isEmpty(jsonResult)){
            throw new BusinessException(MessageConstant.LOGIN_FAILED);
        }


        //获取openid
        JSONObject jsonObject = JSON.parseObject(jsonResult);

        String openid = jsonObject.getString("openid");


        //对openid的非空判断
        if (ObjectUtils.isEmpty(openid)){
            throw new BusinessException(MessageConstant.LOGIN_FAILED);
        }

        User user=userMapper.getByOpenid(openid);


        //判断是否是新用户
        if (user==null){
            //如果是新用户就新建用户

            user = User.builder().openid(openid).createTime(LocalDateTime.now()).build();

            userMapper.insert(user);
        }
        return user;
    }
}
