package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.RedisConstant;
import com.sky.constant.StatusConstant;
import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.entity.DishFlavor;
import com.sky.entity.Setmeal;
import com.sky.exception.BusinessException;
import com.sky.mapper.*;
import com.sky.result.PageResult;
import com.sky.service.DishService;
import com.sky.utils.BeanHelper;
import com.sky.vo.DishVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import java.util.Collections;
import java.util.List;
@Slf4j
@Service
public class DishServiceImpl implements DishService {

    @Autowired
    private SetmealMapper setmealMapper;

    @Autowired
    private DishAndSetmealMapper dishAndSetmealMapper;

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private DishFlavorsMapper dishFlavorsMapper;

    /**
     * 新增菜品
     * @param dishDTO
     */
    @Transactional
    @Override
    public void save(DishDTO dishDTO) {

        Dish dish = BeanHelper.copyProperties(dishDTO, Dish.class);
        dish.setStatus(StatusConstant.DISABLE);
        dishMapper.insert(dish);


        List<DishFlavor> flavors = dishDTO.getFlavors();


        if (!CollectionUtils.isEmpty(flavors)){
            flavors.forEach(e-> e.setDishId(dish.getId()));
            log.info("{}",flavors);
        }

        dishFlavorsMapper.insert(flavors);

    }

    /**
     * 分页查询
     * @return
     * @param dishPageQueryDTO
     */
    @Override
    public PageResult page(DishPageQueryDTO dishPageQueryDTO) {

        PageHelper.startPage(dishPageQueryDTO.getPage(),dishPageQueryDTO.getPageSize());
        List<DishVO> dishDTOList=dishMapper.pageQuery(dishPageQueryDTO);
        Page p = (Page) dishDTOList;
        return new PageResult(p.getTotal(),p.getResult());
    }

    /**
     * 删除
     * @param ids
     */
    @CacheEvict(cacheNames = RedisConstant.DISH,allEntries = true)
    @Override
    @Transactional
    public void delete(List<Long> ids) {

        //起售的菜品不能删除
        Long count= categoryMapper.seacherCount(ids);
        if (count>0){ //说明有菜品正在起售，不能删
            throw new BusinessException(MessageConstant.SETMEAL_ON_SALE);
        }
        //套餐里的菜品不能删除
        Long seacherCount=dishAndSetmealMapper.seacherCount(ids);
        if (seacherCount>0){ //说明有菜品在套餐里，不能删
            throw new BusinessException(MessageConstant.DISH_BE_RELATED_BY_SETMEAL);
        }

        dishFlavorsMapper.delete(ids);

        dishMapper.delete(ids);

    }

    /**
     * 修改菜品
     * @param dishDTO
     */
    @CacheEvict(cacheNames = RedisConstant.DISH,allEntries = true)
    @Override
    @Transactional
    public void update(DishDTO dishDTO) {

        Dish dish = BeanHelper.copyProperties(dishDTO, Dish.class);

        dishMapper.update(dish);

        List<DishFlavor> flavors = dishDTO.getFlavors();

        Long id = dishDTO.getId();
        if (ObjectUtils.isNotEmpty(flavors)){
           dishFlavorsMapper.delete(Collections.singletonList(id));
           flavors.forEach(e-> e.setId(id));
           dishFlavorsMapper.insert(flavors);
        }
    }

    /**
     * 根据id查询菜品
     * @param id
     * @return
     */
    @Override
    public DishVO findById(Long id) {

        Dish dish=dishMapper.findById(id);

        List<DishFlavor> flavors=dishFlavorsMapper.findByDishId(id);

        DishVO dishVO = BeanHelper.copyProperties(dish, DishVO.class);

        dishVO.setFlavors(flavors);

        return dishVO;
    }

    /**
     * 停售和起售
     * @param status
     * @param id
     */
    @CacheEvict(cacheNames = RedisConstant.DISH,allEntries = true)
    @Override
    public void updateStatus(Integer status, Long id) {


        if (status.equals(StatusConstant.DISABLE)){

                List<Long> setmealIds=dishAndSetmealMapper.findSetmealId(id);


                if (ObjectUtils.isNotEmpty(setmealIds)){

                    for (Long setmealId : setmealIds) {
                        Setmeal setmeal = Setmeal.builder().id(setmealId).status(StatusConstant.DISABLE).build();
                        setmealMapper.update(setmeal);
                    }
                }
            }

        Dish dish = Dish.builder().id(id).status(status).build();
        dishMapper.update(dish);
    }

    /**
     * 条件查询菜品列表
     * @param dish
     * @return
     */
    @Override
    public List<Dish> findByConstant(Dish dish) {

        dish.setStatus(StatusConstant.ENABLE);
        List<Dish> dishList=dishMapper.findByConstant(dish);
        return dishList;
    }
}
