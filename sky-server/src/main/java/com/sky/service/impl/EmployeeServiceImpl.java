package com.sky.service.impl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.constant.PasswordConstant;
import com.sky.constant.StatusConstant;
import com.sky.context.BaseContext;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.entity.Employee;
import com.sky.exception.BusinessException;
import com.sky.exception.DataException;
import com.sky.mapper.EmployeeMapper;
import com.sky.result.PageResult;
import com.sky.service.EmployeeService;
import com.sky.utils.BeanHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class EmployeeServiceImpl implements EmployeeService {

    private static final String LOGIN_ADMIN_MESSAGE = "login:admin:";
    private static final String LOGIN_ADMIN_ERROR_MESSAGE = "login:error:";

    @Autowired
    private RedisTemplate<Object,Object> redisTemplate;

    @Autowired
    private EmployeeMapper employeeMapper;

    /**
     * 员工登录
     * @param employeeLoginDTO
     * @return
     */
    @Override
    public Employee employeeLogin(EmployeeLoginDTO employeeLoginDTO) {

        String username = employeeLoginDTO.getUsername();

        //校验账户是否被锁
        Object flag = redisTemplate.opsForValue().get(LOGIN_ADMIN_ERROR_MESSAGE+username);

        if (ObjectUtils.isNotEmpty(flag)){
            Long expire = redisTemplate.getExpire(LOGIN_ADMIN_ERROR_MESSAGE+username);
            log.info("您已经输错5次，账号已锁，请{}秒候再试！",expire);
            throw new BusinessException(MessageConstant.LOGIN_ADMIN_LOCK);
        }

        //参数密码
        String password = employeeLoginDTO.getPassword();

        Employee employee = employeeMapper.employeeLogin(employeeLoginDTO.getUsername());

        log.info("查询返回值 {}",employee);

        //对返回值进行空值判断
        if (employee==null){
            log.info("返回值为空");
            throw new DataException(MessageConstant.ACCOUNT_NOT_FOUND);
        }

        //判断密码是否匹配
        String OkPassword = employee.getPassword();


        //使用md5加密
        password = DigestUtils.md5DigestAsHex(password.getBytes());

        if (!password.equals(OkPassword)){
            //校验账户在五分钟，输入密码错误五次，导致账户被锁

            //设置登录的key
            redisTemplate.opsForValue().set(LOGIN_ADMIN_MESSAGE+username+":"+RandomStringUtils.randomAlphabetic(5),"-",300, TimeUnit.SECONDS);

            System.out.println(LOGIN_ADMIN_MESSAGE+username);

            //计算在五分钟是否输错5次密码
            Set<Object> keys = redisTemplate.keys(LOGIN_ADMIN_MESSAGE + username + ":*");
            if (keys!=null && keys.size()>=5){
                redisTemplate.opsForValue().set(LOGIN_ADMIN_ERROR_MESSAGE+username,"-",1,TimeUnit.HOURS);

                log.info("您已经输错5次，账号已锁，请一个小时候再试！");

            }


            log.info("密码错误");
            throw new BusinessException(MessageConstant.PASSWORD_ERROR);

        }
        //账号失效
        if (employee.getStatus().equals(StatusConstant.DISABLE) ){
            log.info("账号失效");
            throw new BusinessException(MessageConstant.ACCOUNT_LOCKED);
        }

        return employee;
    }

    /**
     * 新增员工
     * @param employeeDTO
     */
    @Override
    public void save(EmployeeDTO employeeDTO) {
        //1.补全实体类
        /*Employee employee = new Employee();
        BeanUtils.copyProperties(employeeDTO,employee);*/
        Employee employee = BeanHelper.copyProperties(employeeDTO, Employee.class);

        //设置默认密码
        employee.setPassword(DigestUtils.md5DigestAsHex(PasswordConstant.DEFAULT_PASSWORD.getBytes()));
        //设置当前时间
//        employee.setCreateTime(LocalDateTime.now());
//        employee.setUpdateTime(LocalDateTime.now());
        //设置默认状态
        employee.setStatus(StatusConstant.ENABLE);

        //设置操作人
//        employee.setCreateUser(BaseContext.getCurrentId());
//        employee.setUpdateUser(BaseContext.getCurrentId());

        employeeMapper.insert(employee);

    }

    /**
     * 分页查询
     * @param queryDTO
     * @return
     */
    @Override
    public PageResult page(EmployeePageQueryDTO queryDTO) {
        //设置分页查询起始页码
        PageHelper.startPage(queryDTO.getPage(),queryDTO.getPageSize());

        List<Employee> employeeList = employeeMapper.list(queryDTO);

        //封装查询结果
        Page p = (Page) employeeList;
        return new PageResult(p.getTotal(),p.getResult());

    }

    /**
     * 启用或禁用员工账号
     * @param status
     * @param id
     */
    @Override
    public void updateStatus(Integer status, Integer id) {
        Employee employee = Employee.builder()
                .id(Long.valueOf(id.toString()))
//                .updateTime(LocalDateTime.now())
//                .updateUser(BaseContext.getCurrentId())
                .status(status)
                .build();

        employeeMapper.update(employee);

    }

    /**
     * 根据id查询员工
     * @param id
     * @return
     */
    @Override
    public Employee findById(Long id) {
        Employee employee=employeeMapper.findById(id);
        return employee;
    }

    /**
     * 编辑员工信息
     * @param employeeDTO
     */
    @Override
    public void update(EmployeeDTO employeeDTO) {

        Employee employee = BeanHelper.copyProperties(employeeDTO, Employee.class);
        employee.setUpdateTime(LocalDateTime.now());
        employee.setUpdateUser(BaseContext.getCurrentId());
        employeeMapper.update(employee);
    }

    /**
     * 修改密码
     * @param passwordEditDTO
     */
    @Override
    public void updatePassword(PasswordEditDTO passwordEditDTO) {
        String oldPassword = passwordEditDTO.getOldPassword();
        oldPassword = DigestUtils.md5DigestAsHex(oldPassword.getBytes());

        Long empId = BaseContext.getCurrentId();
        String Okpassword = employeeMapper.findById(empId).getPassword();

        if (!oldPassword.equals(Okpassword)){
            throw new BusinessException(MessageConstant.PASSWORD_EDIT_FAILED);
        }

        String newPassword = passwordEditDTO.getNewPassword();
        newPassword = DigestUtils.md5DigestAsHex(newPassword.getBytes());
        Employee employee = Employee.builder()
                .id(empId)
//                .updateTime(LocalDateTime.now())
//                .updateUser(BaseContext.getCurrentId())
                .password(newPassword)
                .build();

        employeeMapper.update(employee);

    }
}
