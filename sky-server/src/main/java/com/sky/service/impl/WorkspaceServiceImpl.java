package com.sky.service.impl;

import com.sky.constant.StatusConstant;
import com.sky.dto.UserReportDTO;
import com.sky.entity.Orders;
import com.sky.mapper.DishMapper;
import com.sky.mapper.OrderMapper;
import com.sky.mapper.SetmealMapper;
import com.sky.mapper.UserMapper;
import com.sky.service.WorkspaceService;
import com.sky.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;



@Service
public class WorkspaceServiceImpl implements WorkspaceService {

    @Autowired
    private DishMapper dishMapper;

    @Autowired
    private SetmealMapper setmealMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private OrderMapper orderMapper;


    /**
     * 数据概览
     * @return
     */
    @Override
    public BusinessDataVO businessData() {

        LocalDate now = LocalDate.now();

        LocalDateTime beginTime = LocalDateTime.of(now, LocalTime.MIN);
        LocalDateTime endTime = LocalDateTime.of(now, LocalTime.MAX);


        BusinessDataVO businessDataVO = orderMapper.findAllDetail(beginTime,endTime);

        Integer time = userMapper.findByTime(beginTime, endTime);

        //新增用户数
        businessDataVO.setNewUsers(time);

        return businessDataVO;
    }


    /**
     * 查询订单数据
     * @return
     */
    @Override
    public OrderOverViewVO overviewOrders() {


        OrderOverViewVO orderOverViewVO = new OrderOverViewVO();


        //待接单数量
        Integer waitingOrders = orderMapper.findOrderStatus(Orders.ORDER_STAUTS_TO_BE_CONFIRMED);
        orderOverViewVO.setWaitingOrders(waitingOrders);



        //待派单数量
        Integer deliveredOrders = orderMapper.findOrderStatus(Orders.ORDER_STAUTS_DELIVERY_IN_PROGRESS);
        orderOverViewVO.setDeliveredOrders(deliveredOrders);



        //已完成数量
        Integer completedOrders = orderMapper.findOrderStatus(Orders.ORDER_STAUTS_COMPLETED);
        orderOverViewVO.setCompletedOrders(completedOrders);



        //已取消订单
        Integer cancelledOrders = orderMapper.findOrderStatus(Orders.ORDER_STAUTS_CANCELLED);
        orderOverViewVO.setCancelledOrders(cancelledOrders);


        //查询全部订单
        Integer allOrders = orderMapper.findOrderStatus(null);
        orderOverViewVO.setAllOrders(allOrders);


        return orderOverViewVO;

    }


    /**
     * 查询菜品总览
     * @return
     */
    @Override
    public DishOverViewVO overviewDishes() {


        Integer sold = dishMapper.findByStatus(StatusConstant.ENABLE);

        Integer discontinued = dishMapper.findByStatus(StatusConstant.DISABLE);

        return new DishOverViewVO(sold,discontinued);
    }


    /**
     * 查询套餐总览
     * @return
     */
    @Override
    public SetmealOverViewVO overviewSetmeals() {

        Integer sold = setmealMapper.findByStatus(StatusConstant.ENABLE);

        Integer discontinued = setmealMapper.findByStatus(StatusConstant.DISABLE);



        return new SetmealOverViewVO(sold,discontinued);
    }
}
