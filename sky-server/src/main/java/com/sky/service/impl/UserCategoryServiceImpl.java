package com.sky.service.impl;

import com.sky.constant.RedisConstant;
import com.sky.entity.Category;
import com.sky.mapper.UserCategoryMapper;
import com.sky.service.UserCategoryService;
import io.jsonwebtoken.lang.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserCategoryServiceImpl implements UserCategoryService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private UserCategoryMapper userCategoryMapper;

    /**
     * 分类条件查询
     * @param type
     * @return
     */
    @Override
    public List<Category> findByConstant(Integer type) {

        //1.查询缓存
        List<Category> categorys = (List<Category>) redisTemplate.opsForValue().get(RedisConstant.CATEGORY +type);

        if (!Collections.isEmpty(categorys)){
            return categorys;
        }

        //2.查询数据库
        categorys=userCategoryMapper.findByConstant(type);


        //3.把从数据库查询结果存入缓存

        redisTemplate.opsForValue().set(RedisConstant.CATEGORY+type,categorys);

        return categorys;
    }
}
