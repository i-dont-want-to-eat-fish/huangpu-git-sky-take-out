package com.sky.service.impl;

import com.sky.constant.RedisConstant;
import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.entity.SetmealDish;
import com.sky.mapper.DishAndSetmealMapper;
import com.sky.mapper.UserSetmealMapper;
import com.sky.service.UserSetmealService;
import com.sky.utils.BeanHelper;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserSetmealServiceImpl implements UserSetmealService {

    @Autowired
    private UserSetmealMapper userSetmealMapper;

    @Autowired
    private DishAndSetmealMapper dishAndSetmealMapper;

    /**
     * 根据categoryId查询套餐
     * @param categoryId
     * @return
     */
    @Override
    public List<Setmeal> findByCategoryId(Integer categoryId) {

        List<Setmeal> setmealList=userSetmealMapper.findByCategoryId(categoryId);

        return setmealList;

    }


    /**
     * 根据套餐id查询包含的菜品
     * @param id
     * @return
     */
    @Cacheable(cacheNames = RedisConstant.SETMEAL,key = "#p0")
    @Override
    public List<DishItemVO> findDishBySetmealId(Integer id) {

        List<Long> dishIds=userSetmealMapper.findDishIdBySetmealId(id);


        List<DishItemVO> dishList=userSetmealMapper.findDishByDishId(dishIds);


        return dishList;
    }
}
