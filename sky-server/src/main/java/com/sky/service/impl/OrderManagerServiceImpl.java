package com.sky.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.sky.constant.MessageConstant;
import com.sky.context.BaseContext;
import com.sky.dto.OrdersCancelDTO;
import com.sky.dto.OrdersConfirmDTO;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.dto.OrdersRejectionDTO;
import com.sky.entity.OrderDetail;
import com.sky.entity.Orders;
import com.sky.exception.BusinessException;
import com.sky.mapper.OrderManagerMapper;
import com.sky.mapper.OrderMapper;
import com.sky.result.PageResult;
import com.sky.service.OrderManagerService;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderVO;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderManagerServiceImpl  implements OrderManagerService {

    @Autowired
    private OrderManagerMapper orderManagerMapper;

    @Autowired
    private OrderMapper orderMapper;


    /**
     * 订单搜索
     * @param ordersPageQueryDTO
     * @return
     */
    @Override
    public PageResult page(OrdersPageQueryDTO ordersPageQueryDTO) {

        PageHelper.startPage(ordersPageQueryDTO.getPage(),ordersPageQueryDTO.getPageSize());
        List<OrderVO>  orderVOList = orderManagerMapper.searchOrder(ordersPageQueryDTO);

        if (!ObjectUtils.isEmpty(orderVOList)){
            for (OrderVO orders : orderVOList) {

                String orderId = orders.getNumber();

                List<OrderDetail> orderDetails=orderMapper.findDetailByOrderId(orderId);

                String data="";

                for (OrderDetail orderDetail : orderDetails) {

                    data += orderDetail.getName() + "*" + orderDetail.getNumber();

                }

                orders.setOrderDishes(data);


                orders.setOrderDetailList(orderDetails);
            }
        }


        Page p = (Page) orderVOList;

        return new PageResult(p.getTotal(),p.getResult());
    }

    /**
     * 订单状态统计
     * @return
     */
    @Override
    public OrderStatisticsVO findOrderDetail() {

        Long userId = BaseContext.getCurrentId();


        OrderStatisticsVO orderStatisticsVO=orderManagerMapper.findOrderDetail(userId);

        return orderStatisticsVO;
    }


    /**
     * 查询订单详情
     * @param id
     * @return
     */
    @Override
    public OrderVO findDetail(Long id) {

        OrderVO orderVO = orderMapper.findOrderDetail(id);

        if (ObjectUtils.isNotEmpty(orderVO)){


            String orderId = orderVO.getNumber();

            List<OrderDetail> orderDetails = orderMapper.findDetailByOrderId(orderId);

            orderVO.setOrderDetailList(orderDetails);

        }


        return orderVO;

    }


    /**
     * 接单
     * @param ordersConfirmDTO
     */
    @Override
    public void confirm(OrdersConfirmDTO ordersConfirmDTO) {

        OrderVO orderDetail = orderMapper.findOrderDetail(ordersConfirmDTO.getId());

        if (ObjectUtils.isEmpty(orderDetail)){
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }

        if (orderDetail.getStatus()!=1){   //因为不能支付所以改为只要提交订单就能接单
            throw new BusinessException(MessageConstant.UNKNOWN_ERROR);
        }

        Orders orders = Orders.builder().id(ordersConfirmDTO.getId()).status(3).build();

        orderMapper.update(orders);

    }


    /**
     * 拒单
     * @param ordersRejectionDTO
     */
    @Override
    public void reject(OrdersRejectionDTO ordersRejectionDTO) {

        OrderVO order = orderMapper.findOrderDetail(ordersRejectionDTO.getId());

        if (ObjectUtils.isEmpty(order)){
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }

        if (order.getStatus()!=2){
            throw new BusinessException(MessageConstant.UNKNOWN_ERROR);
        }

        Orders orders = Orders.builder().id(ordersRejectionDTO.getId()).status(6).rejectionReason(ordersRejectionDTO.getRejectionReason()).build();

        orderMapper.update(orders);

        if (order.getPayStatus()==1){

            //TODO  退款操作

            //修改支付状态

            Orders o = Orders.builder().id(ordersRejectionDTO.getId()).payStatus(3).build();

            orderMapper.update(o);

        }

    }


    /**
     * 取消订单
     * @param ordersCancelDTO
     */
    @Override
    public void cancel(OrdersCancelDTO ordersCancelDTO) {

        OrderVO order = orderMapper.findOrderDetail(ordersCancelDTO.getId());

        if (ObjectUtils.isEmpty(order)){
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }


        Orders orders = Orders.builder().id(ordersCancelDTO.getId()).status(6).rejectionReason(ordersCancelDTO.getCancelReason()).build();

        orderMapper.update(orders);

        if (order.getPayStatus()==1){

            //TODO  退款操作

            //修改支付状态

            Orders o = Orders.builder().id(ordersCancelDTO.getId()).payStatus(3).build();

            orderMapper.update(o);

        }

    }


    /**
     * 派送订单
     * @param id
     */
    @Override
    public void delivery(Long id) {

        OrderVO order = orderMapper.findOrderDetail(id);

        if (ObjectUtils.isEmpty(order)){
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }

        if (order.getStatus()!=3){
            throw new BusinessException(MessageConstant.UNKNOWN_ERROR);
        }

        Orders orders = Orders.builder().id(id).status(4).build();

        orderMapper.update(orders);
    }


    /**
     * 完成订单
     * @param id
     */
    @Override
    public void complete(Long id) {

        OrderVO order = orderMapper.findOrderDetail(id);

        if (ObjectUtils.isEmpty(order)){
            throw new BusinessException(MessageConstant.ORDER_NOT_FOUND);
        }

        if (order.getStatus()!=4){
            throw new BusinessException(MessageConstant.UNKNOWN_ERROR);
        }

        Orders orders = Orders.builder().id(id).status(5).build();

        orderMapper.update(orders);

    }
}
