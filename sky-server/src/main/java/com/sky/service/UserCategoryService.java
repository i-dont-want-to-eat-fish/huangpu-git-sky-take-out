package com.sky.service;

import com.sky.entity.Category;

import java.util.List;

public interface UserCategoryService {
    /**
     * 分类条件查询
     * @param type
     * @return
     */
    List<Category> findByConstant(Integer type);
}
