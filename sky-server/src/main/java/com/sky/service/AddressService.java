package com.sky.service;

import com.sky.entity.AddressBook;

import java.util.List;

public interface AddressService {

    /**
     * 新增收货地址
     * @param addressBook
     */
    void add(AddressBook addressBook);


    /**
     * 查询地址
     * @return
     */
    List<AddressBook> find();

    /**
     * 设置默认地址
     * @param addressBook
     */
    void setDefaultAddress(AddressBook addressBook);

    /**
     * 查询默认地址
     * @return
     */
    AddressBook findDefaultAddress();


    /**
     * 根据id查询地址
     * @param id
     * @return
     */
    AddressBook findById(Long id);

    /**
     * 修改收货地址
     * @param addressBook
     */
    void update(AddressBook addressBook);


    /**
     * 根据id删除地址
     * @param addressBook
     */
    void delete(AddressBook addressBook);
}
