package com.sky.service;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;

import java.util.List;

public interface ShoppingService {
    /**
     * 添加购物车
     * @param shoppingCartDTO
     */
    void add(ShoppingCartDTO shoppingCartDTO);


    /**
     * 查询购物车
     * @return
     */
    List<ShoppingCart> find();


    /**
     * 删除商品
     * @param shoppingCart
     */
    void delete(ShoppingCart shoppingCart);


    /**
     * 清空购物车
     */
    void deleteAll();
}
