package com.sky.service;

import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.vo.DishItemVO;
import com.sky.vo.DishVO;
import com.sky.vo.SetmealVO;

import java.util.List;

public interface UserSetmealService {
    /**
     * 根据categoryId查询套餐
     * @param categoryId
     * @return
     */
    List<Setmeal> findByCategoryId(Integer categoryId);

    /**
     * 根据套餐id查询包含的菜品
     * @param id
     * @return
     */
    List<DishItemVO> findDishBySetmealId(Integer id);
}
