package com.sky.service;

public interface ShopService {
    /**
     * 设置店铺运营状态
     * @param status
     */
    void updateStatus(Integer status);

    /**
     * 店铺运营状态
     */
    Integer getStatus();
}
