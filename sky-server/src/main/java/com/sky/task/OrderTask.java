package com.sky.task;

import com.sky.entity.Orders;
import com.sky.mapper.OrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Component
public class OrderTask {

    @Autowired
    private OrderMapper orderMapper;

    /**
     * 定时关闭超时未支付的订单
     */
    @Scheduled(cron = "0/30 * * * * ?")   //每隔30秒执行一次
    public void cancelOrder(){

        //1、查询符合条件（待支付，15分钟前下单）的订单
        Integer status = Orders.ORDER_STAUTS_PENDING_PAYMENT;

        LocalDateTime beforeTime = LocalDateTime.now().minusMinutes(15);

        List<Orders> ordersList=orderMapper.findByStatusAndTime(status,beforeTime);

        //2、取消符合查询的订单

        if (ObjectUtils.isNotEmpty(ordersList)){

            for (Orders orders : ordersList) {

                orders.setStatus(Orders.ORDER_STAUTS_CANCELLED);

                orderMapper.update(orders);

            }

        }

    }




    @Scheduled(cron = "0 0 1 * * ?")  //凌晨一点执行
    public void completeOrder(){

        //1、查询符合条件（待支付，15分钟前下单）的订单
        Integer status = Orders.ORDER_STAUTS_DELIVERY_IN_PROGRESS;

        LocalDateTime beforeTime = LocalDateTime.now().minusHours(2);

        List<Orders> ordersList=orderMapper.findByStatusAndTime(status,beforeTime);

        //2、取消符合查询的订单

        if (ObjectUtils.isNotEmpty(ordersList)){

            for (Orders orders : ordersList) {

                orders.setStatus(Orders.ORDER_STAUTS_COMPLETED);

                orderMapper.update(orders);

            }

        }

    }


}
