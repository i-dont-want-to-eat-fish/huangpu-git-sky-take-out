package com.sky.socket;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
@ServerEndpoint("/ws/{sid}")  //标识当前类就是处理webSocket请求
public class WebSocket {

    private static Map<String,Session> sessionMap = new HashMap<>(); //所有与服务器建立连接的会话对象

    @OnOpen //该方法建立时触发
    public void onOpen(Session session, @PathParam("sid") String sid){
        log.info("连接建立，{}",session);
        sessionMap.put(sid, session);
    }

    @OnMessage  //该方法接收到客户端消息时触发
    public void onMessage(Session session ,String message, @PathParam("sid") String sid){
        log.info("接收到消息，{}",message);
    }

    @OnClose  //该方法断开连接时触发
    public void onClose(Session session , @PathParam("sid") String sid){
        log.info("关闭连接，{}",session);
        sessionMap.remove(sid);
    }

    @OnError //该方法通信异常触发
    public void onError(Session session , @PathParam("sid") String sid,Throwable throwable){
        log.info("出异常了，{}",session);
        throwable.printStackTrace();

    }

    /**
     * 广播消息
     */
    public void sendMessageToAllClient(String message) throws Exception {

        Collection<Session> sessions = sessionMap.values();

        if (!CollectionUtils.isEmpty(sessions)){
            for (Session session : sessions) {
                //发送消息
                session.getBasicRemote().sendText(message);
            }
        }

    }

}
