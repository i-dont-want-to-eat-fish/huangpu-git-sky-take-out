package com.sky.controller.admin;

import com.sky.constant.JwtClaimsConstant;
import com.sky.dto.EmployeeDTO;
import com.sky.dto.EmployeeLoginDTO;
import com.sky.dto.EmployeePageQueryDTO;
import com.sky.dto.PasswordEditDTO;
import com.sky.entity.Employee;
import com.sky.properties.JwtProperties;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.EmployeeService;
import com.sky.utils.JwtUtil;
import com.sky.vo.EmployeeLoginVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 员工管理Controller
 */
@Slf4j
@Api(tags = "员工登录")
@RestController
@RequestMapping("/admin/employee")
public class EmployeeController {
    @Autowired
    private JwtProperties jwtProperties;

    @Autowired
    private EmployeeService employeeService;

    /**
     * 员工登录
     * @param employeeLoginDTO
     * @return
     */
    @ApiOperation("员工登录")
    @PostMapping("/login")
    public Result<EmployeeLoginVO> employeeLogin(@RequestBody EmployeeLoginDTO employeeLoginDTO){
        log.info("参数封装信息 {}",employeeLoginDTO);

        //调用Service 的方法
        Employee employee =employeeService.employeeLogin(employeeLoginDTO);
        log.info("调用service返回值{}",employee);

        //检验成功下发令牌

        Map<String,Object> claims = new HashMap<>();
        claims.put(JwtClaimsConstant.EMP_ID,employee.getId());

        String jwt = JwtUtil.createJWT(jwtProperties.getAdminSecretKey(), jwtProperties.getAdminTtl(), claims);

        EmployeeLoginVO employeeLoginVO = EmployeeLoginVO.builder().id(employee.getId()).userName(employee.getUsername()).name(employee.getName()).token(jwt).build();

        return Result.success(employeeLoginVO);
    }

    /**
     * 新增员工
     * @param employeeDTO
     * @return
     */
    @ApiOperation("新增员工")
    @PostMapping
    public Result save(@RequestBody EmployeeDTO employeeDTO){
        log.info("传递参数{}",employeeDTO);

        employeeService.save(employeeDTO);
        return Result.success();

    }

    /**
     * 分页查询
     * @param queryDTO
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public Result<PageResult> list (EmployeePageQueryDTO queryDTO){
        log.info("分页查询参数{}",queryDTO);
        PageResult pageResult= employeeService.page(queryDTO);
        return Result.success(pageResult);
    }

    /**
     * 启用或禁用员工账号
     * @param status
     * @param id
     * @return
     */
    @PutMapping("status/{status}/{id}")
    public Result updateStatus(@PathVariable Integer status,@PathVariable Integer id){
        log.info("路径参数为{} ，{}",status,id);

        employeeService.updateStatus(status,id);

        return Result.success();
    }

    /**
     * 根据id查询员工
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<Employee> findById(@PathVariable Long id){
        log.info("查询的ID：{}",id);
        Employee employee= employeeService.findById(id);

        return Result.success(employee);
    }

    /**
     * 编辑员工信息
     * @param employeeDTO
     * @return
     */
    @PutMapping()
    public Result update(@RequestBody EmployeeDTO employeeDTO){
        log.info("修改的信息{}",employeeDTO);

        employeeService.update(employeeDTO);
        return Result.success();
    }

    /**
     * 修改密码
     * @param passwordEditDTO
     * @return
     */
    @PutMapping("/editPassword")
    public Result updatePassword(@RequestBody PasswordEditDTO passwordEditDTO){
        log.info("修改密码的参数信息{}",passwordEditDTO);

        employeeService.updatePassword(passwordEditDTO);
        return Result.success();
    }


}
