package com.sky.controller.admin;

import com.sky.dto.DishDTO;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.DishService;
import com.sky.vo.DishVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/admin/dish")
public class DishController {

    @Autowired
    private DishService dishService;

    /**
     * 新增菜品
     * @param dishDTO
     * @return
     */
    @PostMapping
    public Result save(@RequestBody DishDTO dishDTO){

        dishService.save(dishDTO);

        return Result.success();
    }

    /**
     * 分页查询
     * @param dishPageQueryDTO
     * @return
     */
    @GetMapping("/page")
    public Result<PageResult> page(DishPageQueryDTO dishPageQueryDTO){
        log.info("开始分页查询{}",dishPageQueryDTO);

        PageResult pageResult=dishService.page(dishPageQueryDTO);

        return Result.success(pageResult);

    }

    /**
     * 删除
     * @param ids
     * @return
     */
    @DeleteMapping
    public Result delete(@RequestParam List<Long> ids){
        dishService.delete(ids);
        return Result.success();
    }

    /**
     * 根据id查询菜品
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result<DishVO> findById(@PathVariable Long id){
        DishVO dishVO=dishService.findById(id);
        return Result.success(dishVO);
    }

    /**
     * 菜品更新
     * @param dishDTO
     * @return
     */
    @PutMapping
    public Result update(@RequestBody DishDTO dishDTO){

        dishService.update(dishDTO);
        return Result.success();
    }

    /**
     * 停售和起售
     * @param status
     * @param id
     * @return
     */
    @PutMapping("/status/{status}/{id}")
    public Result upDatetatus(@PathVariable Integer status,@PathVariable Long id){
        dishService.updateStatus(status,id);
        return Result.success();
    }

    /**
     * 条件查询菜品列表
     * @return
     */
    @GetMapping("/list")
    public Result findByConstant(Dish dish){

        List<Dish> dishList=dishService.findByConstant(dish);

        return Result.success(dishList);

    }
}
