package com.sky.controller.admin;

import com.sky.dto.CategoryDTO;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Slf4j
@Api(tags="分类管理")
@RestController
@RequestMapping("/admin/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;


    /**
     * 分类分页查询
     * @param categoryPageQueryDTO
     * @return
     */
    @ApiOperation("分页查询")
    @GetMapping("/page")
    public Result<PageResult> findByConstant(CategoryPageQueryDTO categoryPageQueryDTO){
        log.info("{}",categoryPageQueryDTO);

        PageResult pageResult=categoryService.findByConstant(categoryPageQueryDTO);

        return Result.success(pageResult);
    }

    /**
     * 根据id查询员工
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result findById(@PathVariable Long id){
        log.info("路径信息{}",id);
        Category category=categoryService.findById(id);
        return Result.success(category);

    }

    /**
     * 修改分类
     * @param categoryDTO
     * @return
     */
    @PutMapping
    public Result update(@RequestBody CategoryDTO categoryDTO){

        categoryService.update(categoryDTO);

        return Result.success();
    }

    /**
     * 更新状态
     * @param status
     * @param id
     * @return
     */
    @PutMapping("status/{status}/{id}")
    public Result updateStatus(@PathVariable Integer status,@PathVariable Long id){
        log.info("路径参数为{} ，{}",status,id);

        categoryService.updateStatus(status,id);
        return Result.success();
    }

    /**
     * 新增分类
     * @param categoryDTO
     * @return
     */
    @PostMapping
    public Result save(@RequestBody CategoryDTO categoryDTO){

        categoryService.save(categoryDTO);
        return Result.success();

    }

    /**
     * 删除分类
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public Result delete(@PathVariable Long id){
        categoryService.delete(id);
        return Result.success();
    }


    /**
     * 根据类型查询分类
     * @param type
     * @return
     */
    @GetMapping("/list")
    public Result findByType(Integer type){

        List<Category> categoryList=categoryService.findByType(type);

        return Result.success(categoryList);
    }
}
