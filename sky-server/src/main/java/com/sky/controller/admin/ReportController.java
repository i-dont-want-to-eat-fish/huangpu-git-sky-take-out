package com.sky.controller.admin;


import com.sky.result.Result;
import com.sky.service.ReportService;
import com.sky.vo.OrderReportVO;
import com.sky.vo.SalesTop10ReportVO;
import com.sky.vo.TurnoverReportVO;
import com.sky.vo.UserReportVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;


@Slf4j
@RestController
@RequestMapping("/admin/report")
public class ReportController {

    @Autowired
    private ReportService reportService;


    /**
     * 营业额统计接口
     * @param begin
     * @param end
     * @return
     */
    @GetMapping("/turnoverStatistics")
    public Result turnoverStatistics(@DateTimeFormat(pattern = "yyyy-MM-dd")  LocalDate begin,
                                     @DateTimeFormat(pattern = "yyyy-MM-dd")  LocalDate end){
        log.info("统计营业额，开始时间：{} 结束时间：{}",begin,end);

        TurnoverReportVO turnoverReportVO= reportService.turnoverStatistics(begin,end);

        return Result.success(turnoverReportVO);

    }

    /**
     * 用户统计接口
     * @param begin
     * @param end
     * @return
     */
    @GetMapping("/userStatistics")
    public Result userStatistics(@DateTimeFormat(pattern = "yyyy-MM-dd")  LocalDate begin,
                                     @DateTimeFormat(pattern = "yyyy-MM-dd")  LocalDate end){
        log.info("用户统计，开始时间：{} 结束时间：{}",begin,end);

        UserReportVO userReportVO= reportService.userStatistics(begin,end);

        return Result.success(userReportVO);

    }


    /**
     * 订单统计
     * @param begin
     * @param end
     * @return
     */
    @GetMapping("/ordersStatistics")
    public Result ordersStatistics(@DateTimeFormat(pattern = "yyyy-MM-dd")  LocalDate begin,
                                 @DateTimeFormat(pattern = "yyyy-MM-dd")  LocalDate end){
        log.info("订单统计，开始时间：{} 结束时间：{}",begin,end);

        OrderReportVO orderReportVO= reportService.ordersStatistics(begin,end);

        return Result.success(orderReportVO);

    }


    /**
     * 查询销量排名top10
     * @param begin
     * @param end
     * @return
     */
    @GetMapping("/top10")
    public Result top(@DateTimeFormat(pattern = "yyyy-MM-dd")  LocalDate begin,
                                   @DateTimeFormat(pattern = "yyyy-MM-dd")  LocalDate end){
        log.info("查询销量排名top10，开始时间：{} 结束时间：{}",begin,end);

        SalesTop10ReportVO salesTop10ReportVO= reportService.top(begin,end);

        return Result.success(salesTop10ReportVO);

    }


    /**
     * 导出Excel报表
     */
    @GetMapping("/export")
    public void export() throws Exception {

        reportService.export();

    }

}
