package com.sky.controller.admin;

import com.sky.dto.*;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.OrderManagerService;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/admin/order")
public class OrderManagerController {

    @Autowired
    private OrderManagerService orderManagerService;


    /**
     * 订单搜索
     * @param ordersPageQueryDTO
     * @return
     */
    @GetMapping("/conditionSearch")
    public Result page(OrdersPageQueryDTO ordersPageQueryDTO){

        PageResult  pageResult=orderManagerService.page(ordersPageQueryDTO);

        return Result.success(pageResult);

    }

    /**
     * 订单状态统计
     * @return
     */
    @GetMapping("/statistics")
    public Result findOrderDetail(){

        OrderStatisticsVO orderDetail=orderManagerService.findOrderDetail();

        return Result.success(orderDetail);
    }


    /**
     * 查询订单详情
     * @return
     */
    @GetMapping("/details/{id}")
    public Result findDetail(@PathVariable Long id){

        OrderVO  orderVO =orderManagerService.findDetail(id);

        return Result.success(orderVO);

    }


    /**
     * 接单
     * @param ordersConfirmDTO
     * @return
     */
    @PutMapping("/confirm")
    public Result confirm(@RequestBody OrdersConfirmDTO ordersConfirmDTO){
        log.info("确认订单{}",ordersConfirmDTO);

        orderManagerService.confirm(ordersConfirmDTO);

        return Result.success();

    }

    /**
     * 拒单
     * @param ordersRejectionDTO
     * @return
     */
    @PutMapping("/rejection")
    public Result reject(@RequestBody OrdersRejectionDTO ordersRejectionDTO){

        orderManagerService.reject(ordersRejectionDTO);

        return Result.success();
    }


    /**
     * 取消订单
     * @param ordersCancelDTO
     * @return
     */
    @PutMapping("/cancel")
    public Result cancel(@RequestBody OrdersCancelDTO ordersCancelDTO){

        orderManagerService.cancel(ordersCancelDTO);

        return Result.success();

    }


    /**
     * 派送订单
     * @param id
     * @return
     */
    @PutMapping("/delivery/{id}")
    public Result delivery(@PathVariable Long id){

        orderManagerService.delivery(id);

        return Result.success();
    }


    /**
     * 完成订单
     * @param id
     * @return
     */
    @PutMapping("/complete/{id}")
    public Result complete(@PathVariable Long id){

        orderManagerService.complete(id);

        return Result.success();
    }

}
