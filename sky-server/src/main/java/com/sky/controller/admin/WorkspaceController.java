package com.sky.controller.admin;


import com.sky.result.Result;
import com.sky.service.WorkspaceService;
import com.sky.vo.BusinessDataVO;
import com.sky.vo.DishOverViewVO;
import com.sky.vo.OrderOverViewVO;
import com.sky.vo.SetmealOverViewVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/admin/workspace")
public class WorkspaceController {


    @Autowired
    private WorkspaceService workspaceService;


    /**
     * 数据概览
     * @return
     */
    @GetMapping("/businessData")
    public Result businessData(){

        BusinessDataVO businessDataVO=workspaceService.businessData();

        return Result.success(businessDataVO);

    }


    /**
     * 查询今日订单数据
     * @return
     */
    @GetMapping("/overviewOrders")
    public Result overviewOrders(){

        OrderOverViewVO orderOverViewVO=workspaceService.overviewOrders();

        return Result.success(orderOverViewVO);

    }

    /**
     * 查询菜品总览
     * @return
     */
    @GetMapping("/overviewDishes")
    public Result overviewDishes(){

        DishOverViewVO dishOverViewVO=workspaceService.overviewDishes();

        return Result.success(dishOverViewVO);

    }

    /**
     * 查询套餐总览
     * @return
     */
    @GetMapping("/overviewSetmeals")
    public Result overviewSetmeals(){

        SetmealOverViewVO setmealOverViewVO=workspaceService.overviewSetmeals();

        return Result.success(setmealOverViewVO);

    }


}
