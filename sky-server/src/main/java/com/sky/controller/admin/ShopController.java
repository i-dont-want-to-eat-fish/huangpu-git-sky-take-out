package com.sky.controller.admin;

import com.sky.result.Result;
import com.sky.service.ShopService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/admin/shop")
public class ShopController {

    @Autowired
    private ShopService shopService;

    /**
     * 设置店铺运营状态
     * @param status
     * @return
     */
    @PutMapping("/{status}")
    public Result updateStatus(@PathVariable Integer status){
        log.info("开始设置店铺运营状态：{}",status);
        shopService.updateStatus(status);

        return Result.success();
    }

    /**
     * 获取店铺运营状态
     * @return
     */
    @GetMapping("/status")
    public Result getStatus(){
        log.info("获得店铺运营状态");
        Integer status=shopService.getStatus();
        return Result.success(status);
    }
}
