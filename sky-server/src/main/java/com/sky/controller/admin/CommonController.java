package com.sky.controller.admin;

import com.sky.result.Result;
import com.sky.utils.AliOssUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/admin/common")
public class CommonController {

    @Autowired
    private AliOssUtil aliOssUtil;

    @PostMapping("/upload")
    public Result upload(MultipartFile file) throws Exception {

        UUID uuid = UUID.randomUUID();

        String originalFilename = file.getOriginalFilename();

        String extendName = originalFilename.substring(originalFilename.lastIndexOf("."));

        String newName=uuid+extendName;

        String url = aliOssUtil.upload(file.getBytes(), newName);

        return Result.success(url);
    }

}

