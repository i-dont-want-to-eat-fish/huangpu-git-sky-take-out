package com.sky.controller.user;

import com.sky.dto.ShoppingCartDTO;
import com.sky.entity.ShoppingCart;
import com.sky.result.Result;
import com.sky.service.ShoppingService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Delete;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/user/shoppingCart")
public class ShoppingController {
    @Autowired
    private ShoppingService shoppingService;


    /**
     * 添加购物车
     * @param shoppingCartDTO
     * @return
     */
    @PostMapping("/add")
    public Result add(@RequestBody ShoppingCartDTO shoppingCartDTO){
        log.info("开始添加购物车,{}",shoppingCartDTO);
        shoppingService.add(shoppingCartDTO);

        return Result.success();
    }


    /**
     * 查询购物车
     * @return
     */
    @GetMapping("/list")
    public Result find(){

        List<ShoppingCart> shoppingCartList=shoppingService.find();

        return Result.success(shoppingCartList);
    }

    /**
     * 删除商品
     * @param shoppingCart
     * @return
     */
    @PostMapping("/sub")
    public Result delete(@RequestBody ShoppingCart shoppingCart){

        shoppingService.delete(shoppingCart);

        return Result.success();

    }

    /**
     * 清空购物车
     * @return
     */
    @DeleteMapping("/clean")
    public Result deleteAll(){

        shoppingService.deleteAll();

        return Result.success();

    }



}
