package com.sky.controller.user;


import com.sky.result.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@Api(tags = "c端-店铺操作接口")
public class ClientShopController {

    @Autowired
    private RedisTemplate redisTemplate;

    private static final String SHOP_STATUS_KEY = "status";


    @ApiOperation("查询店铺营业状态")
    @GetMapping("/user/shop/status")
    public Result getStatus(){

        Integer status= (Integer)redisTemplate.opsForValue().get(SHOP_STATUS_KEY);

        log.info("查询店铺的营业状态为: {}", status == 1?"营业中":"打样中");

        return Result.success(status);

    }
}
