package com.sky.controller.user;

import com.sky.entity.Category;
import com.sky.result.Result;
import com.sky.service.UserCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@Api(tags = "C端-分类接口")
@RequestMapping("/user/category")
public class UserCategoryController {

    @Autowired
    private UserCategoryService userCategoryService;


    /**
     * 分类条件查询
     * @param type
     * @return
     */
    @ApiOperation("分类条件查询")
    @GetMapping("/list")
    public Result<List<Category>> findByConstant(Integer type){

        log.info("分类条件查询{}",type);

        List<Category> categorys=userCategoryService.findByConstant(type);

        return Result.success(categorys);

    }
}
