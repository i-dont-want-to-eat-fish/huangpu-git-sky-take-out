package com.sky.controller.user;

import com.sky.entity.Dish;
import com.sky.result.Result;
import com.sky.service.UserDishService;
import com.sky.vo.DishVO;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@Api(tags = "C端-菜品浏览接口")
@RequestMapping("/user/dish")
public class UserDishController {

    @Autowired
    public UserDishService userDishService;

    /**
     * 根据分类id查询菜品
     * @param categoryId
     * @return
     */
    @GetMapping("/list")
    public Result<List<DishVO>> findDishByCategoryId(Integer categoryId){

        List<DishVO> dishVOs=userDishService.findDishByCategoryId(categoryId);

        return Result.success(dishVOs);
    }
}
