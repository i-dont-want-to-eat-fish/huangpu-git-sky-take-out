package com.sky.controller.user;


import com.alibaba.fastjson.JSONObject;
import com.sky.dto.OrdersPageQueryDTO;
import com.sky.dto.OrdersPaymentDTO;
import com.sky.dto.OrdersSubmitDTO;
import com.sky.result.PageResult;
import com.sky.result.Result;
import com.sky.service.OrderService;
import com.sky.socket.WebSocket;
import com.sky.vo.OrderPaymentVO;
import com.sky.vo.OrderSubmitVO;
import com.sky.vo.OrderVO;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("/user/order")
public class OrderController {

    @Autowired
    private OrderService orderService;


    @Autowired
    private WebSocket webSocket;


    /**
     * 提交订单
     * @param ordersSubmitDTO
     * @return
     */
    @PostMapping("/submit")
    public Result<OrderSubmitVO> submit(@RequestBody OrdersSubmitDTO ordersSubmitDTO) throws Exception {
        log.info("开始支付{}",ordersSubmitDTO);

        OrderSubmitVO orderSubmitVO= orderService.submit(ordersSubmitDTO);

        return Result.success(orderSubmitVO);

    }


    /**
     * 订单支付
     *
     * @param ordersPaymentDTO
     * @return
     */
    @PutMapping("/payment")
    @ApiOperation("订单支付")
    public Result<OrderPaymentVO> payment(@RequestBody OrdersPaymentDTO ordersPaymentDTO) throws Exception {
        log.info("订单支付：{}", ordersPaymentDTO);
        OrderPaymentVO orderPaymentVO = orderService.payment(ordersPaymentDTO);
        log.info("生成预支付交易单：{}", orderPaymentVO);
        return Result.success(orderPaymentVO);
    }


    /**
     * 查询历史订单记录
     * @param ordersPageQueryDTO
     * @return
     */
    @GetMapping("/historyOrders")
    public Result<PageResult> findOrder(OrdersPageQueryDTO ordersPageQueryDTO){
        log.info("开始查询历史记录");

        PageResult pageResult = orderService.findOrder(ordersPageQueryDTO);

        return Result.success(pageResult);

    }

    /**
     * 查询订单详情
     * @param id
     * @return
     */
    @GetMapping("/orderDetail/{id}")
    public Result findOrderDetail(@PathVariable Long id){

        OrderVO orderVO=orderService.findOrderDetail(id);

        return Result.success(orderVO);
    }


    /**
     * 取消订单
     * @param id
     * @return
     */
    @PutMapping("/cancel/{id}")
    public Result cancel(@PathVariable Long id){

        orderService.cancel(id);

        return Result.success();

    }


    /**
     * 再来一单
     * @param id
     * @return
     */
    @PostMapping("/repetition/{id}")
    public Result again(@PathVariable Long id){

        orderService.again(id);

        return Result.success();

    }


    @GetMapping("/reminder/{id}")
    public Result reminder(@PathVariable Long id) throws Exception {


        OrderVO orderVO = orderService.findOrderDetail(id);

        if (ObjectUtils.isNotEmpty(orderVO)){

            //提醒用户接单
            Map<String,Object> paramMap = new HashMap<>();
            paramMap.put("type",1);
            paramMap.put("orderId",id);
            paramMap.put("content",orderVO.getNumber());

            webSocket.sendMessageToAllClient(JSONObject.toJSONString(paramMap));
        }

        return Result.success();

    }

}
