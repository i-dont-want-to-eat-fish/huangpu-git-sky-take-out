package com.sky.controller.user;

import com.sky.entity.AddressBook;
import com.sky.result.Result;
import com.sky.service.AddressService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/user/addressBook")
public class AddressController {

    @Autowired
    private AddressService addressService;


    /**
     * 新增收货地址
     * @param addressBook
     * @return
     */
    @PostMapping
    public Result add(@RequestBody AddressBook addressBook){

        log.info("新增收货地址:{}",addressBook);
        addressService.add(addressBook);

        return Result.success();
    }

    /**
     * 查询地址
     * @return
     */
    @GetMapping("/list")
    public Result find(){

        List<AddressBook> addressBookList =addressService.find();

        return Result.success(addressBookList);
    }


    /**
     * 设置默认地址
     * @param addressBook
     * @return
     */
    @PutMapping("/default")
    public Result setDefaultAddress(@RequestBody AddressBook addressBook){

        addressService.setDefaultAddress(addressBook);

        return Result.success();

    }


    /**
     * 查询默认地址
     * @return
     */
    @GetMapping("/default")
    public Result findDefaultAddress(){

        AddressBook addressBook = addressService.findDefaultAddress();

        return Result.success(addressBook);
    }


    /**
     * 根据id查询地址
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Result findById(@PathVariable Long id){

        AddressBook addressBook = addressService.findById(id);

        return Result.success(addressBook);

    }

    /**
     * 修改收货地址
     * @param addressBook
     * @return
     */
    @PutMapping
    public Result update(@RequestBody AddressBook addressBook){

        addressService.update(addressBook);

        return Result.success();
    }

    /**
     * 根据id删除地址
     * @return
     */
    @DeleteMapping
    public Result delete(@RequestBody AddressBook addressBook){

        addressService.delete(addressBook);

        return Result.success();

    }

}
