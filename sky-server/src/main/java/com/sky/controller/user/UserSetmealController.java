package com.sky.controller.user;

import com.sky.entity.Dish;
import com.sky.entity.Setmeal;
import com.sky.result.Result;
import com.sky.service.UserSetmealService;
import com.sky.vo.DishItemVO;
import com.sky.vo.SetmealVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/user/setmeal")
public class UserSetmealController {

    @Autowired
    private UserSetmealService userSetmealService;

    /**
     * 根据categoryId查询套餐
     * @param categoryId
     * @return
     */
    @GetMapping("/list")
    public Result<List<Setmeal>> findByCategoryId(Integer categoryId){
        log.info("根据categoryId查询套餐");
        List<Setmeal> setmealList=userSetmealService.findByCategoryId(categoryId);

        return Result.success(setmealList);

    }


    /**
     * 根据套餐id查询包含的菜品
     * @param id
     * @return
     */
    @GetMapping("dish/{id}")
    public Result<List<DishItemVO>> findDishBySetmealId(@PathVariable Integer id){

        List<DishItemVO> dishItemVO =userSetmealService.findDishBySetmealId(id);

        return Result.success(dishItemVO);

    }
}
