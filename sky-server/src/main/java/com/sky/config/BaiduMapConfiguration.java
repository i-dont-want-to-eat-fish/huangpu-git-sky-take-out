package com.sky.config;

import com.sky.properties.BaiduMapProperties;
import com.sky.utils.BaiduMapUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BaiduMapConfiguration {


    //全局Bean注入
    @Bean
    public BaiduMapUtils baiduMapUtils(BaiduMapProperties baiduMapProperties){

        return new BaiduMapUtils(baiduMapProperties);

    }
}
