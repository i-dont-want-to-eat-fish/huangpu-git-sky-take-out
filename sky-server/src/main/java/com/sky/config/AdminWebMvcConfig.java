package com.sky.config;
import com.sky.interceptor.AdminLoginInterceptor;
import com.sky.interceptor.ClientLoginInterceptor;
import com.sky.json.JacksonObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;
@Slf4j
@Configuration
public class AdminWebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private AdminLoginInterceptor adminLoginInterceptor;

    @Autowired
    private ClientLoginInterceptor clientLoginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //浏览器登录拦截
        registry.addInterceptor(adminLoginInterceptor).addPathPatterns("/admin/**").excludePathPatterns("/admin/employee/login");
        //小程序登录拦截
        registry.addInterceptor(clientLoginInterceptor).addPathPatterns("/user/**").excludePathPatterns("/user/user/login","/user/shop/status");
    }

    /**
     * 自定义JSON转换器
     * @param converters
     */
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        log.info("扩展消息转换器");

        //MappingJackson2CborHttpMessageConverter messageConverter = new MappingJackson2CborHttpMessageConverter();  错误
        MappingJackson2HttpMessageConverter messageConverter = new MappingJackson2HttpMessageConverter();

        messageConverter.setObjectMapper(new JacksonObjectMapper());

        converters.add(0,messageConverter);

    }

}
