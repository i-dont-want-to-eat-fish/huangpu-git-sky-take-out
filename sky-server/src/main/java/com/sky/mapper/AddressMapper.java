package com.sky.mapper;

import com.sky.entity.AddressBook;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface AddressMapper {

    /**
     * 新增收货地址
     * @param addressBook
     */
    @Insert("insert into address_book(user_id, consignee, sex, phone, province_code, province_name, city_code, city_name, district_code, district_name, detail, label, is_default, create_time) " +
            "values (#{userId}, #{consignee}, #{sex}, #{phone}, #{provinceCode}, #{provinceName}, #{cityCode}, #{cityName}, #{districtCode}, #{districtName}, #{detail}, #{label}, #{isDefault}, #{createTime})")
    void add(AddressBook addressBook);


    /**
     * 查询地址
     * @param addressBook
     * @return
     */

    List<AddressBook> find(AddressBook addressBook);

    /**
     * 修改地址
     * @param addressBook
     */
    void update(AddressBook addressBook);


    /**
     * 查询默认地址
     * @param addressBook
     * @return
     */
    @Select("select id, user_id, consignee, sex, phone, province_code, province_name, city_code, city_name, district_code, district_name, detail, label, is_default, create_time from address_book where user_id = #{userId} and is_default= #{isDefault}")
    AddressBook findDefaultAddress(AddressBook addressBook);


    /**
     * 根据id删除地址
     * @param addressBook
     */
    @Delete("delete from address_book where user_id = #{userId} and id =#{id}")
    void delete(AddressBook addressBook);
}
