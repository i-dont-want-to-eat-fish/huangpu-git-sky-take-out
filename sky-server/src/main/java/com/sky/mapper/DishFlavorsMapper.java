package com.sky.mapper;

import com.sky.entity.DishFlavor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DishFlavorsMapper {

    void insert(List<DishFlavor> flavors);

    void delete(List<Long> ids);

    @Select("select id, dish_id, name, value from dish_flavor where dish_id =#{id}")
    List<DishFlavor> findByDishId(Long id);
}
