package com.sky.mapper;

import com.sky.annotation.AutoFill;
import com.sky.dto.SetmealPageQueryDTO;
import com.sky.entity.Setmeal;
import com.sky.enumeration.OperationType;
import com.sky.vo.SetmealVO;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SetmealMapper {

    @Select("select s.id, s.name, s.category_id, s.price, s.status, s.description, s.image, s.create_time, s.update_time, s.create_user, s.update_user, c.id, c.type, c.name, c.sort, c.status, c.create_time, c.update_time, c.create_user, c.update_user from setmeal s left join category c on c.id=s.category_id where c.id =#{id}")
    List<Setmeal> seacherSetmeal(Long id);


    /**
     * 修改套餐
     * @param setmeal
     */
    void update(Setmeal setmeal);


    @Insert("insert into setmeal (name, category_id, price, status, description, image, create_time, update_time, create_user, update_user) " +
            "values (#{name}, #{categoryId}, #{price}, #{status}, #{description}, #{image}, #{createTime}, #{updateTime}, #{createUser}, #{updateUser})")
    @AutoFill(OperationType.INSERT)
    @Options(useGeneratedKeys = true,keyProperty = "id")
    void save(Setmeal setmeal);

    /**
     * 分页查询
     * @param setmealPageQueryDTO
     * @return
     */
    List<SetmealVO> page(SetmealPageQueryDTO setmealPageQueryDTO);

    /**
     * 批量删除套餐
     * @param ids
     */
    void deleteById(List<Long> ids);


    SetmealVO seacherSetmealById(List<Long> ids);


    /**
     * 查询套餐总览
     * @param status
     * @return
     */

    @Select("select count(*) from setmeal where  status =#{status}")
    Integer findByStatus(Integer status);
}
