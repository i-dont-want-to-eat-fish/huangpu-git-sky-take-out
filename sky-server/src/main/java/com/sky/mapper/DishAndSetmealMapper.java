package com.sky.mapper;

import com.sky.entity.SetmealDish;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DishAndSetmealMapper {

    Long seacherCount(List<Long> ids);

    /**
     *
     * @param id
     * @return
     */
    @Select("select s.id from dish d , setmeal s ,setmeal_dish sd where d.id=sd.dish_id and s.id= sd.setmeal_id and dish_id =#{id}")
    List<Long> findSetmealId(Long id);


    /**
     * 新增套餐
     * @param setmealDishes
     */
    void save(List<SetmealDish> setmealDishes);


  /*  List<Long> findsetmealIdBySetmealId(List<Long> ids);*/

    /**
     * 删除菜品和套餐关联表的信息
     * @param ids
     */
    void deleteBysetmealIds(List<Long> ids);


    /**
     * 根据id查询套餐
     * @param id
     * @return
     */
    @Select("select id, setmeal_id, dish_id, name, price, copies from setmeal_dish where setmeal_id=#{id}")
    List<SetmealDish> findBySetmealId(Long id);

    @Select("select  dish_id from setmeal_dish where setmeal_id = #{id}")
    List<Long> seacherDishId(Long id);
}
