package com.sky.mapper;

import com.sky.entity.Setmeal;
import com.sky.vo.DishItemVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserSetmealMapper {

    /**
     * 根据categoryId查询套餐
     * @param categoryId
     * @return
     */

    @Select("select id, name, category_id, price, status, description, image, create_time, update_time, create_user, update_user from setmeal where category_id = #{category_id} and status=1")
    List<Setmeal> findByCategoryId(Integer categoryId);

    /**
     * 通过套餐id查找菜品
     * @param id
     * @return
     */
    @Select("select sd.dish_id from  setmeal s ,setmeal_dish sd   where s.id=sd.setmeal_id and s.status=1 and s.id=#{id}")
    List<Long> findDishIdBySetmealId(Integer id);


    /**
     * 通过dishid查询dish
     * @param dishIds
     * @return
     */
    List<DishItemVO> findDishByDishId(List<Long> dishIds);

}
