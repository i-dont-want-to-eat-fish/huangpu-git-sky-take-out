package com.sky.mapper;

import com.sky.entity.Dish;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserDishMapper {


    /**
     * 通过分类id查询菜品信息（只査起售的菜品）
     * @param categoryId
     * @return
     */
    @Select("select id, name, category_id, price, image, description, status, create_time, update_time, create_user, update_user from dish where category_id =#{categoryId} and status=1")
    List<Dish> findDishByCategoryId(Integer categoryId);

}
