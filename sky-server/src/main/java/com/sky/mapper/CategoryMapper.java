package com.sky.mapper;

import com.sky.annotation.AutoFill;
import com.sky.dto.CategoryPageQueryDTO;
import com.sky.entity.Category;
import com.sky.enumeration.OperationType;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CategoryMapper {

    /**
     * 分类分页查询
     * @param categoryPageQueryDTO
     * @return
     */
    List<Category> findByConstant(CategoryPageQueryDTO categoryPageQueryDTO);

    /**
     * 根据id查询员工
     * @param id
     * @return
     */
    @Select("select id, type, name, sort, status, create_time, update_time, create_user, update_user from category where id =#{id}")
    Category findById(Long id);

    /**
     * 修改分类
     * @param category
     */
    @AutoFill(OperationType.UPDATE)
    void update(Category category);

    /**
     * 新增分类
     * @param category
     */
    @AutoFill(OperationType.INSERT)
    void insert(Category category);

    /**
     * 删除分类
     * @param id
     */
    @Delete("delete from category where id =#{id}")
    void delete(Long id);



    /**
     * 根据类型查询分类
     * @param type
     * @return
     */
    @Select("select id, type, name, sort, status, create_time, update_time, create_user, update_user from category where type=#{type}")
    List<Category> findByType(Integer type);


    Long seacherCount(List<Long> ids);
}
