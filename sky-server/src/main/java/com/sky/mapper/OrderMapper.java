package com.sky.mapper;

import com.sky.dto.OrderReportDTO;
import com.sky.dto.SalesReportDTO;
import com.sky.dto.TurnoverReportDTO;
import com.sky.entity.OrderDetail;
import com.sky.entity.Orders;
import com.sky.vo.BusinessDataVO;
import com.sky.vo.OrderReportVO;
import com.sky.vo.OrderVO;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface OrderMapper {


    /**
     * 新增订单表
     * @param orders
     */
    @Insert("insert into orders ( number, status, user_id, address_book_id, order_time, pay_method, pay_status, amount, remark, phone, address, consignee,estimated_delivery_time, pack_amount, tableware_number, tableware_status) " +
            "values (#{number}, #{status}, #{userId}, #{addressBookId}, #{orderTime}, #{payMethod}, #{payStatus}, #{amount}, #{remark}, #{phone}, #{address}, #{consignee}, #{estimatedDeliveryTime},#{packAmount}, #{tablewareNumber}, #{tablewareStatus})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    void insertOrders(Orders orders);


    @Insert("insert into order_detail (order_id, dish_id, setmeal_id, name, image, dish_flavor, number, amount) " +
            "values (#{orderId}, #{dishId}, #{setmealId}, #{name}, #{image}, #{dishFlavor}, #{number}, #{amount})")
    void insertOrderDetail(OrderDetail orderDetail);



    /**
     * 根据订单号和用户id查询订单
     */
    @Select("select * from orders where number = #{orderNumber}")
    Orders getByNumber(String orderNumber);


    /**
     * 修改订单信息
     * @param orders
     */
    void update(Orders orders);


    /**
     * 根据状态查询
     * @param status
     * @return
     */

    List<OrderVO> findOrder(Integer status);


    @Select("select id, order_id, dish_id, setmeal_id, name, image, dish_flavor, number, amount from order_detail where order_id =#{orderId}")
    List<OrderDetail> findDetailByOrderId(String number);


    @Select("select id, number, status, user_id, address_book_id, order_time, checkout_time, pay_method, pay_status, amount, remark, phone, address, consignee, cancel_reason, rejection_reason, cancel_time, estimated_delivery_time, delivery_status, delivery_time, pack_amount, tableware_number, tableware_status from orders where id =#{id}")
    OrderVO findOrderDetail(Long id);


    /**
     * 通过时间和状态查询订单
     * @param status
     * @param beforeTime
     * @return
     */
    @Select("select id, number, status, user_id, address_book_id, order_time, checkout_time, pay_method, pay_status, amount, remark, phone, address, consignee, cancel_reason, rejection_reason, cancel_time, estimated_delivery_time, delivery_status, delivery_time, pack_amount, tableware_number, tableware_status from orders where status =#{status} and order_time<= #{beforeTime}")
    List<Orders> findByStatusAndTime(Integer status, LocalDateTime beforeTime);


    /**
     * 查询营业额
     * @param beginTime
     * @param endTime
     * @param status
     * @return
     */
    List<TurnoverReportDTO> findTurnoverByDate(LocalDateTime beginTime, LocalDateTime endTime, Integer status);


    /**
     * 计算每天的订单数
     * @param beginTime
     * @param endTime
     * @param status
     * @return
     */
    List<OrderReportDTO> findOrderList(LocalDateTime beginTime, LocalDateTime endTime, Integer status);


    /**
     * 查询有效订单中菜品的排名
     * @param beginTime
     * @param endTime
     * @param status
     * @return
     */
    @Select("select od.name goodsName,\n" +
            "       sum(od.number)  goodsNumber from orders o,order_detail od\n" +
            "where   o.number=od.order_id and o.order_time between #{beginTime} and #{endTime} and o.status=#{status}  group by goodsName order by goodsNumber desc")
    List<SalesReportDTO> findValidDish(LocalDateTime beginTime, LocalDateTime endTime, Integer status);


    /**
     * 查询状态
     * @param stauts
     * @return
     */
    Integer findOrderStatus(Integer stauts);


    /**
     * 查询订单的所有明细
     * @param beginTime
     * @param endTime
     * @return
     */
    @Select("select  ifnull(sum(amount),0) turnover ,ifnull(sum(if(status=5,1,0)),0) validOrderCount, ifnull(sum(if(status=5,1,0))/count(*),0) orderCompletionRate  , ifnull(count(amount)/ sum(if(status=5,1,0)),0) unitPrice from orders where order_time between #{beginTime} and #{endTime}")
    BusinessDataVO findAllDetail(LocalDateTime beginTime, LocalDateTime endTime);
}
