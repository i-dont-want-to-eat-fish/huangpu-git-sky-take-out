package com.sky.mapper;

import com.sky.annotation.AutoFill;
import com.sky.dto.DishPageQueryDTO;
import com.sky.entity.Dish;
import com.sky.enumeration.OperationType;
import com.sky.vo.DishVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface DishMapper {

    /**
     * 新增菜品
     * @param dish
     */
    @AutoFill(OperationType.INSERT)
    public void insert(Dish dish);


    @Select("select d.id, d.name, d.category_id, d.price, d.image, d.description, d.status, d.create_time, d.update_time, d.create_user, d.update_user, c.id, c.type, c.name, c.sort, c.status, c.create_time, c.update_time, c.create_user, c.update_user from dish d left join category c on c.id=d.category_id  where c.id =#{id}")
    List<Dish> seacherDish(Long id);

    /**
     * 分页查询
     * @param dishPageQueryDTO
     * @return
     */

    List<DishVO> pageQuery(DishPageQueryDTO dishPageQueryDTO);

    void delete(List<Long> ids);

    void update(Dish dish);

    @Select("select ID, NAME, CATEGORY_ID, PRICE, IMAGE, DESCRIPTION, STATUS, CREATE_TIME, UPDATE_TIME, CREATE_USER, UPDATE_USER from dish where id=#{id}")
    Dish findById(Long id);

    /**
     * 条件查询菜品列表
     * @return
     * @param dish
     */
    List<Dish> findByConstant(Dish dish);

    /**
     * 该状态下的商品数量
     * @param status
     * @return
     */
    @Select("select count(*) from dish where  status =#{status}")
    Integer findByStatus(Integer status);
}
