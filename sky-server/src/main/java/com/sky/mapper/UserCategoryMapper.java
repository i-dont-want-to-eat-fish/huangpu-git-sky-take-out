package com.sky.mapper;

import com.sky.entity.Category;
import org.apache.ibatis.annotations.Mapper;


import java.util.List;

@Mapper
public interface UserCategoryMapper {
    /**
     * 分类条件查询
     * @param type
     * @return
     */

    List<Category> findByConstant(Integer type);
}
