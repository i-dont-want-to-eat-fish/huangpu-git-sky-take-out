package com.sky.mapper;

import com.sky.dto.OrdersPageQueryDTO;
import com.sky.vo.OrderStatisticsVO;
import com.sky.vo.OrderVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;


import java.util.List;

@Mapper
public interface OrderManagerMapper {

    /**
     * 根据条件查询订单
     * @param ordersPageQueryDTO
     * @return
     */
    List<OrderVO> searchOrder(OrdersPageQueryDTO ordersPageQueryDTO);

    /**
     * 订单状态统计
     * @return
     * @param userId
     */
    @Select("select  sum(case status when 2 then 1 end ) toBeConfirmed ,sum(case status when 3 then 1 end ) confirmed , sum(case status when 4 then 1 end ) deliveryInProgress from orders where user_id=#{userId}")
    OrderStatisticsVO findOrderDetail(Long userId);
}
