package com.sky.mapper;

import com.sky.dto.UserReportDTO;
import com.sky.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface UserMapper {

    /**
     * 通openid查询用户信息
     * @param openid
     * @return
     */
    @Options(useGeneratedKeys = true,keyProperty = "id")
    @Select("select id, openid, name, phone, sex, id_number, avatar, create_time from user where openid =#{openid}")
    User getByOpenid(String openid);


    /**
     * 插入新用户信息
     * @param user
     */
    @Insert("insert into user(openid, name, phone, sex, id_number, avatar, create_time) values (#{openid},#{name},#{phone},#{sex},#{idNumber},#{avatar},#{createTime})")
    void insert(User user);

    /**
     * 通过id查询User
     * @param userId
     * @return
     */
    @Select("select id, openid, name, phone, sex, id_number, avatar, create_time from user where id=#{id}")
    User getById(Long userId);

    /**
     * 获取按指定时间的新增用户
     * @param beginTime
     * @return
     */
    List<UserReportDTO> getNewUserListByDate(LocalDateTime beginTime, LocalDateTime endTime);


    @Select("select ifnull(count(*) ,0) userCount from user where create_time < #{beginTime}")
    Integer getUserListBeforeTime(LocalDateTime beginTime);


    @Select("select ifnull(count(*) ,0) userCount from user where create_time < #{endTime} and create_time >#{beginTime}")
    Integer findByTime(LocalDateTime beginTime, LocalDateTime endTime);
}
