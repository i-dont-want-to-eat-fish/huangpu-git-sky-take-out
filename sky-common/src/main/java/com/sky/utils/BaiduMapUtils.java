package com.sky.utils;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.sky.constant.MessageConstant;
import com.sky.exception.BusinessException;
import com.sky.properties.BaiduMapProperties;
import com.sky.result.LocalAddress;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.util.HashMap;
import java.util.Map;


@Data
@Slf4j
@AllArgsConstructor
public class BaiduMapUtils {


    private BaiduMapProperties baiduMapProperties;


    //获取经纬度
    public LocalAddress getAddress(String address){
        log.info("{}",address);

        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("address",address);
        paramMap.put("output","json");
        paramMap.put("ak",baiduMapProperties.getAk());


        String result = HttpClientUtil.doGet(baiduMapProperties.getGeoCoderUrl(), paramMap);

        if (ObjectUtils.isEmpty(result)){
            throw new BusinessException(MessageConstant.ADDRESS_PARSE_ERROR);
        }

        JSONObject jsonObject = JSON.parseObject(result);
        Integer status = jsonObject.getInteger("status");

        if (status != 0){
            throw new BusinessException(MessageConstant.ADDRESS_PARSE_ERROR);
        }

        String lat = jsonObject.getJSONObject("result").getJSONObject("location").getString("lat");
        String lng = jsonObject.getJSONObject("result").getJSONObject("location").getString("lng");

        return new LocalAddress(lng,lat);
    }





    public Integer getDistance(LocalAddress origin,LocalAddress destination){

        //发送http请求, 获取位置对应的坐标
        Map<String, String> paramMap = new HashMap<>();
        paramMap.put("ak", baiduMapProperties.getAk());
        paramMap.put("origin", origin.getLatLng());
        paramMap.put("destination", destination.getLatLng());

        String result = HttpClientUtil.doGet(baiduMapProperties.getDrivingUrl(), paramMap);


        JSONObject jsonObject = JSON.parseObject(result);
        if (ObjectUtils.isEmpty(result)){
            throw new BusinessException(MessageConstant.ADDRESS_PARSE_ERROR);
        }

        Integer status = jsonObject.getInteger("status");

        if (status != 0){
            throw new BusinessException(MessageConstant.ADDRESS_PARSE_ERROR);
        }

        JSONObject routes =(JSONObject) jsonObject.getJSONObject("result").getJSONArray("routes").get(0);

        Integer distance = routes.getInteger("distance");

        return distance;

    }


   /* //拼接 geocoding的URL
    //https://api.map.baidu.com/geocoding/v3/?output=json&callback=showLocation&ak=4NaGKDLe6kZvCBjGkeEn6ZL8NsEYLG7p&address=湖北省武汉市黄陂区青龙路传智播客教育科创园
    //https://api.map.baidu.com/geocoding/v3/?&output=json&callback=showLocation&address=北京市海淀区上地十街10号&ak=您的ak //GET请求
    public String getGeoCoderUrl(String address) {
        StringBuilder sb = new StringBuilder();
        sb.append(baiduMapProperties.getGeoCoderUrl());
        sb.append("&address=").append(address);
        sb.append("&ak=").append(baiduMapProperties.getAk());
        log.info("请求路径:{}", sb);
        return sb.toString();
    }

    //https://api.map.baidu.com/directionlite/v1/driving?origin=40.01116,116.339303&destination=39.936404,116.452562&ak=您的AK
    //https://api.map.baidu.com/directionlite/v1/riding?&originlat=30.71326903396565,113.69857697641786&destination=34.62898498374996,113.69857697641786&ak=4NaGKDLe6kZvCBjGkeEn6ZL8NsEYLG7p
    //拼接 directionlite的 url
    public String getDirectionListURL(String originlat, String originlng, String destinationlat, String destinationlng) {
        StringBuilder sb = new StringBuilder();
        sb.append(baiduMapProperties.getDrivingUrl()).append("&origin=").append(originlat).append(",").append(originlng);
        sb.append("&destination=").append(destinationlat).append(",").append(destinationlng);
        sb.append("&ak=").append(baiduMapProperties.getAk());
        return sb.toString();
    }

    //经纬度
    public Map<String, String> getLitAndLng(String origin) {
        String geoCoderUrl = getGeoCoderUrl(origin);
        String orginResult = HttpClientUtil.doGet(geoCoderUrl, null);
        String jsonData = orginResult.substring(orginResult.indexOf("{"), orginResult.lastIndexOf("}") + 1); // 提取有效的JSON数据部分

        JSONObject result = JSON.parseObject(jsonData).getJSONObject("result").getJSONObject("location");
        log.info("坐班经纬度 -> result:{}", result);
        Map<String, String> stringHashMap = new HashMap<>();

        stringHashMap.put("lng", result.get("lng").toString());
        stringHashMap.put("lat", result.get("lat").toString());
        return stringHashMap;
    }

    public Long getDistionList(String origin, String distination) {
        Map<String, String> originLatAndLng = getLitAndLng(origin);
        Map<String, String> distinationLatAndLng = getLitAndLng(distination);
        String listUrl = getDirectionListURL(originLatAndLng.get("lat"), distinationLatAndLng.get("lng"), distinationLatAndLng.get("lat"), distinationLatAndLng.get("lng"));
        String list = HttpClientUtil.doGet(listUrl, null);
        JSONObject result = JSON.parseObject(list).getJSONObject("result").getJSONArray("routes").getJSONObject(0);
        Object distance = result.get("distance");
        return Long.valueOf(distance.toString());
    }*/
}
