package com.sky.constant;

/**
 * 缓存常量
 */
public class RedisConstant {

    public static final String CATEGORY="category:redis";

    public static final String DISH="dish:redis";

    public static final String SETMEAL="setmeal:redis";

}
