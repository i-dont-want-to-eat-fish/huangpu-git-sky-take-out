package com.sky.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LocalAddress implements Serializable {

    private String lng;   //经度

    private String lat;   //纬度


    public String getLatLng(){
        return lat + "," + lng;
    }

}
