package com.sky.interceptor;
import com.sky.constant.JwtClaimsConstant;
import com.sky.context.BaseContext;
import com.sky.properties.JwtProperties;
import com.sky.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Slf4j
@Component

public class AdminLoginInterceptor implements HandlerInterceptor {

    @Autowired
    private JwtProperties jwtProperties;


    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //拿到url地址值
        String url = request.getRequestURL().toString();

        //拿到jwt令牌
        String token = request.getHeader("token");

        //判断taken是否为空
        if (!StringUtils.hasLength("token")){
            log.info("jwt令牌为空,响应401");
            response.setStatus(HttpStatus.SC_UNAUTHORIZED);
            return false;
        }

        try {
            Claims claims = JwtUtil.parseJWT(jwtProperties.getAdminSecretKey(), token);

            Long id = Long.valueOf(claims.get(JwtClaimsConstant.EMP_ID).toString());
            //储存当前用户id
            BaseContext.setCurrentId(id);

        } catch (Exception e) {
            e.printStackTrace();

            log.info("jwt令牌解析失败,响应401");

            response.setStatus(HttpStatus.SC_UNAUTHORIZED);
            return false;
        }
        return true;
    }

    //删除ThreadLocal，避免造成内存泄露
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        BaseContext.removeCurrentId();
    }
}
