package com.sky.aspect;

import com.sky.annotation.AutoFill;
import com.sky.constant.AutoFillConstant;
import com.sky.context.BaseContext;
import com.sky.enumeration.OperationType;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Method;
import java.time.LocalDateTime;

@Slf4j
@Aspect
@Component
public class AutoFillAspect {

    @Before("execution(* com.sky.mapper.*.*(..)) && @annotation(autoFill)")
    private void autoFillProperty(JoinPoint joinPoint,AutoFill autoFill) throws Exception {
        log.info("进入AOP，开始赋值");

        //1.获取原始方法的第一个参数
        Object[] args = joinPoint.getArgs();
        if (ObjectUtils.isEmpty(args)){ //参数为空
            return;
        }

        Object arg = args[0];

        log.info("为公共属性开始赋值，赋值前{}",arg);

        //通过反射获取方法对象
        Method setCreateTime = arg.getClass().getDeclaredMethod(AutoFillConstant.SET_CREATE_TIME, LocalDateTime.class);
        Method setCreateUser = arg.getClass().getDeclaredMethod(AutoFillConstant.SET_CREATE_USER, BaseContext.getCurrentId().getClass());
        Method setUpdateTime = arg.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_TIME, LocalDateTime.class);
        Method setUpdateUser = arg.getClass().getDeclaredMethod(AutoFillConstant.SET_UPDATE_USER, BaseContext.getCurrentId().getClass());

        //获取value属性
        /*Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        AutoFill autoFill = methodSignature.getMethod().getAnnotation(AutoFill.class);*/

        OperationType operationType = autoFill.value();

        //判断value的属性
        if (operationType.equals(OperationType.INSERT)){
            setCreateTime.invoke(arg,LocalDateTime.now());
            setCreateUser.invoke(arg, BaseContext.getCurrentId());
        }
        setUpdateTime.invoke(arg,LocalDateTime.now());
        setUpdateUser.invoke(arg,BaseContext.getCurrentId());

        log.info("为公共属性开始赋值，赋值后{}",arg);
    }

}
